#!/usr/bin/python

import serial
import threading
import time
import linkingStorage as ls

ser = serial.Serial('/dev/ttyACM0', 921600)                                 # create and open serial port 
print "serial port opened"
ls.commPortOpened = True

class SerialMessaging ( threading.Thread ):

    def __init__( self ):
        threading.Thread.__init__( self )
        self.stopEvent = threading.Event()

    def stop( self ):
        self.stopEvent.set()

    def isStopped( self ):
        return self.stopEvent.is_set()

    def run( self ):
        while not self.isStopped():
            if ls.SEND is True:
                ls.SEND = False
                if ls.sendCode == 'T':
                    stringValue = str(ls.turnTarget)
                elif ls.sendCode == 'S':
                    stringValue = str(ls.speedTarget)
                elif ls.sendCode == 'D':
                    stringValue = str(ls.distanceTarget)
                elif ls.sendCode == 'A':
                    stringValue = str(ls.KpPitch)
                elif ls.sendCode == 'B':
                    stringValue = str(ls.KdPitch)
                elif ls.sendCode == 'E':
                    stringValue = str(ls.KpHor)
                elif ls.sendCode == 'F':
                    stringValue = str(ls.KdHor)
                elif ls.sendCode == 'X':
                    self.stop()
                    break
                else:
                    stringValue = str(0)
                messageOut = ls.sendCode + stringValue + ls.marker
                ser.write(messageOut)
            self.getMessage()

    def getMessage(self):
        if  ser.inWaiting() > 1:
            opCodeIn = ser.read()
            messageIn = "" 
            charIn = ser.read()
            while charIn != ls.marker:
                messageIn += charIn
                charIn = ser.read()
            if opCodeIn == 'a':
                ls.KpPitch = self.useFloat(messageIn)
            elif opCodeIn == 'b':
                ls.KdPitch = self.useFloat(messageIn)
            elif opCodeIn == 'C':
                ls.CONNECTION = True
            elif opCodeIn == 'd':
                ls.distanceMade = self.useFloat(messageIn)
            elif opCodeIn == 'e':
                ls.KpHor = self.useFloat(messageIn)
            elif opCodeIn == 'f':
                ls.KdHor = self.useFloat(messageIn)
            elif opCodeIn == 'g':
                ls.pitchTarget = self.useFloat(messageIn)
            elif opCodeIn == 'h':
                ls.heading = self.useFloat(messageIn)
            elif opCodeIn == 'j':
                ls.pitchError = self.useFloat(messageIn)
            elif opCodeIn == 'k':
                ls.robotState = messageIn
            elif opCodeIn == 'l':
                ls.tickError = self.useFloat(messageIn)
            elif opCodeIn == 'm':
                ls.tickTarget = self.useFloat(messageIn)
            elif opCodeIn == 's':
                ls.speedActual = self.useFloat(messageIn)
            elif opCodeIn == 'y':
                ls.declination = self.useFloat(messageIn)
            ls.RECIEVED = True
            ser.flushOutput()

    def useInteger(self, messageIn):
        try:
            test = int(messageIn)
            return test
        except ValueError:
            return 9999

    def useFloat(self, messageIn):
        try:
            test = float(messageIn)
            return test
        except ValueError:
            return 9999.9

messageThread = SerialMessaging()                                       # create and start thread
messageThread.start()
print "Serial messaging thread started"
ls.commThreadStarted = True

time.sleep(20)                                                          # Unfortunately Arduino restarts when port is opened
                                                                        # So wait until IMU is aganin calibrated (> 15s)
if ser.isOpen() is True:                                                # haling
    ls.sendCode = 'c'
    ls.SEND = True
    print "hail send to Arduino"

#print "waiting for response ",
while ls.CONNECTION is False:
     #print "|",
     continue
    
#print " "
print "connected to Arduino"
