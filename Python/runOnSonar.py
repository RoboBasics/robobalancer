#!/usr/bin/python

############################################################# 
# g.data:   0 = target distance
#           1 = distance made
#           2 = heading
#           3 = 
#           4 = sonar side  left   90 degrees
#           5 = sonar front left   70 degrees
#           6 = sonar front left   90 degrees
#           7 = sonar front middle 90 degrees
#           8 = sonar front right  90 degrees
#           9 = sonar front right  70 degrees
#          10 = sonar side  right  90 degrees
#
# c.bounds: 0 = min side distance (prevent to lean to a wall)
#           1 = min front 70 degerees
#           2 = min frontal distance 
#           3 = min distance 2 move
############################################################# 

import sys
import time
import json
import control as rc
import sonar

us = sonar.UltraSonic()

class constants(object): pass                                                    # Create a 'struct' for constants
c = constants()
class globals(object): pass                                                      # Create a 'struct' for global variables
g = globals()

c.bounds = [0.15, 0.50, 0.40,0.6] 
c.file = '/home/pi/Balancer/debugOut-'+str((int(time.time())%10000))+'.txt'

g.Operational = False
g.runStart    = time.clock()                                                     # For 'Watchdog' timer; switches g.Operational
g.timerStart  = time.clock()                                                     # For timer to prevent an overload of the Arduino
g.data        = [0.0] * 11                                                       # Holds distanceMade, distanceTarget, bearing, sennsors
g.lastTurn    = 0

debugLog = open (c.file, 'w')                                                    # Create an empty log file 
debugLog.close()

def mainExit():
    rc.stopThreading()
    sys.exit()    

def runFailure():
    rc.setDistanceTarget(0)
    print "runTimer expired without an actual speed"

    debugLog = open (c.file, 'a')
    debugLog.write(' \n')
    debugLog.write(str(g.data))
    debugLog.close()

    rbState = rc.getRobotState()
    if rbState == 'D':
        print "RB status = Horizontal"
    answer = raw_input("Start new trajectory?")
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
        answer = raw_input("Start new trajectory?")
    if answer == 'n' or answer == 'N':
        g.Operational = False
        mainExit()  
    else:
        initTrajectory()

def backout():
    rc.setDistanceTarget(0.2)
    rc.setSpeed(-11)
    rbState = rc.getRobotState()
    while rbState != 'F':
        rbState = rc.getRobotState()
    #rc.checkBalance()

def initTrajectory():
    'Waits until bot is upright and performs a fresh start of a run'

    print ("state RB = Initializing")
    rbState = rc.getRobotState()
    if rbState == 'D':
        print "RB status = Horizontal"
    while rbState == 'D':
        rbState = rc.getRobotState()
    print "RB status = Vertical"
   
    debugLog = open (c.file, 'a')
    debugLog.write('New trajectory ====' )
    debugLog.close()

    g.Operational = True
    g.data = [0.0] * 11

    g.data[2] = rc.getHeading()

    raw_input("Hit enter")
    #newLeg()
   
def makeTurn(angle):                                                             # Move to control.py after testing
    'Turn angle degrees left or right and wait until turn is almost finished'
    g.data[2] += angle
    rc.setTurn(angle)
    heading = rc.getHeading()
    while not g.data[2] - 1 <= heading <= g.data[2] + 1:
        heading = rc.getHeading()

def newLeg():
    'Save data, assess position and determine new path'
 
    g.data[1] = rc.getDistanceMade()
    
    debugLog = open (c.file, 'a')
    debugLog.write(' \n')
    debugLog.write(str(g.data))
    debugLog.close()

    rc.checkBalance()

    legFound = False
    g.lastTurn = 0
    Left = -1
    Right = 1

    while not legFound:
        g.data[2] = rc.getHeading()
        g.data[4:] = rc.getSonarReading()
        print g.data

        if g.data[7] <= g.innerBoundFront:                                       # No room to turn; need to back-out (FF90) 
            backout()

        elif g.data[7] <= g.c.bounds[2]:                                     # No room to run and stop (FF90)
            if g.data[4] > g.data[10]:                                           # Check side sensors (SL90 & SR90) 
                makeTurn(Left * 45)
            else:
                makeTurn(Right * 45) 

        elif g.data[6] <= g.c.bounds[2]:                                     # Check left front for passage width (FL90)
            if g.lastTurn < 0:                                                   # Prevent reversing last turn
                makeTurn(Left * 45)
            else:
                makeTurn(Right * 45)

        elif g.data[8] <= g.c.bounds[2]:                                     # Check right front for passage width (FR90)
            if g.lastTurn > 0:       
                makeTurn(Right * 45) 
            else:
                makeTurn(Left * 45)             

        elif g.data[5] <= g.c.bounds[1]:                                     # Check (a bit) in between front sensors (FL70)
             if g.lastTurn < 0:
                makeTurn(Left * 45) 
             else:
                makeTurn(Right * 45)

        elif g.data[9] <= g.c.bounds[1]:                                     # (FR70)
             if g.lastTurn > 0:            
                makeTurn(Right * 45) 
             else:
                makeTurn(Left * 45)              

        elif g.data[4] <= g.c.bounds[0]:                                      # Check if not too close to a wall (SL90)
             makeTurn(Right * 45)
             
        elif g.data[10] <= g.c.bounds[0]:                                     # (SR90)
             makeTurn(Left * 45)              

        elif (min(g.data[6:9]) - g.c.bounds[2]) <= g.c.bounds[3]:           # Only start if free space makes sense
            if g.data[4] > g.data[10]:
                makeTurn(Left * 45)
            else:
                makeTurn(Right * 45)
        
        else:
            legFound = True

            g.data[0] = (min(g.data[6:9]) - g.c.bounds[2])                   # Set distanceTarget in meters
            rc.setDistanceTarget(g.data[0])
            
            g.data[1] = 0.0                                                      # Not nescessary

            #if g.data[3] <= g.outerBoundS90:                                    # WIP - to build in: stick-to-one-hand mechanism 
            #    g.wallAtLeft = True
            #if g.data[9] <= g.outerBoundS90:
            #    g.wallAtRight = True

            g.data[2] = rc.getHeading()                                          # g.data[2] = int(round(g.data[2],0))
            g.data[3] = 0
            rc.setSpeed(11)
            print g.data            
            g.runStart = time.clock()                                            # (Re-)start 'watchdog' in case the bot gets stuck somewhere

def writeLogging():
    debugLog = open (c.file, 'a')
    debugLog.write(' \n')
    debugLog.write(str(g.data))
    debugLog.close()

def singleTrack():
    g.data[4:] = us.getSonarReading()
    g.data[0] = round((sum(g.data[6:9])/3 - g.c.bounds[2]),2)
    rc.setDistanceTarget(g.data[0])
    g.data[1] = 0.0
    g.data[2] = rc.getHeading()                                          
    # g.data[2] = int(round(g.data[2],0))
    g.data[3] = 0
    writeLogging()
    rc.setSpeed(11)
    rbState = rc.getRobotState()
    while rbState != 'F':
        rbState = rc.getRobotState()
    g.data[4:] = us.getSonarReading()
    g.data[1] = rc.getDistanceMade()
    writeLogging()
    rc.checkBalance()
    g.data[4:] = us.getSonarReading()
    g.data[1] = rc.getDistanceMade()
    writeLogging()

 
if __name__ == "__main__":
    initTrajectory()
    singleTrack()
    print "Finished"
    rc.stopThreading()
    sys.exit()    


    '''
    while g.Operational:
        rbState = rc.getRobotState()
        while rbState != 'F':                                                    # distanceMade < distanceTarget
            g.data[4:] = rc.getSonarReading()                                    # Security status check
            
            if min(g.data[6:9]) <= g.innerBoundFront:                            # Unexpected obstacle in front
                rc.setSpeed(0)
                g.data[3] = 1                                                    # Record the event
                rbState = 'F'
            
            if g.data[4] <= g.c.bounds[0] or g.data[10] <= g.c.bounds[0]: # Running towards a wall
                rc.setSpeed(0)
                g.data[3] = 2
                rbState = 'F'
            elif (time.clock() - g.timerStart) >= 0.2:
                rbState = rc.getRobotState()
                print rbState
                g.timerStart = time.clock()    
            elif (time.clock() - g.runStart) > 10:
                if rbState != 'F':
                    speed = rc.getSpeed()
                    if speed > 0.01 or speed < -0.01:
                        g.runStart = time.clock()
                    else:
                        runFailure()
        newLeg()
 '''