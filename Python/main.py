#!/usr/bin/python

import sys
import time
import json
import control as rc

class globals(object): pass                                                      # Create a 'struct' for global variables
g = globals()

g.Operational        = False
g.runStart       = time.clock()                                                  # For 'Watchdog' timer; switches g.Operational
g.timerStart     = time.clock()                                                  # For timer to prevent an overload of the Arduino 
g.outerBoundFront = 0.3                                                          # Leave room to stop and maneuver
g.innerBoundFront = 0.16                                                         # No room to turn (min half of bot width needed )
g.innerBoundAngle = 0.5                                                          # A minimum of 49.7cm on both 70 degr sensors is needed for the width of the bot 
g.innerBoundSide  = 0.15                                                         # Keep distance from wall to enable differential steering
g.minDist2Move    = 0.5    
g.data           = [0.0] * 11                                                    # Holds distanceMade, distanceTarget, bearing, sennsors
g.lastTurn       = 0
g.debugFilename  = '/home/pi/Balancer/debugOut-'+str((int(time.time())%100))+'.txt'

debugLog         = open (g.debugFilename, 'w')                                   # Create an empty log file 
debugLog.close()

def mainExit():
    rc.stopThreading()
    sys.exit()    

def runFailure():
    rc.setDistanceTarget(0)
    print "runTimer expired without an actual speed"

    debugLog = open (g.debugFilename, 'a')
    debugLog.write(' \n')
    debugLog.write(str(g.data))
    debugLog.close()

    rbState = rc.getRobotState()
    if rbState == 'D':
        print "RB status = Horizontal"
    answer = raw_input("Start new trajectory?")
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
        answer = raw_input("Start new trajectory?")
    if answer == 'n' or answer == 'N':
        g.Operational = False
        mainExit()  
    else:
        initTrajectory()

def backout():
    rc.setDistanceTarget(0.2)
    rc.setSpeed(-11)
    rbState = rc.getRobotState()
    while rbState != 'F':
        rbState = rc.getRobotState()
    #rc.checkBalance()

def initTrajectory():
    'Waits until bot is upright and performs a fresh start of a run'

    print ("state RB = Initializing")
    rbState = rc.getRobotState()
    if rbState == 'D':
        print "RB status = Horizontal"
    while rbState == 'D':
        rbState = rc.getRobotState()
    print "RB status = Vertical"
   
    debugLog = open (g.debugFilename, 'a')
    debugLog.write('New trajectory ====' )
    debugLog.close()

    g.Operational = True
    g.data = [0.0] * 11

    g.data[2] = rc.getHeading()

    raw_input("Hit enter")
    newLeg()
   
def makeTurn(angle):                                                             # Move to control.py after testing
    'Turn angle degrees left or right and wait until turn is almost finished'
    g.data[2] += angle
    rc.setTurn(angle)
    heading = rc.getHeading()
    while not g.data[2] - 1 <= heading <= g.data[2] + 1:
        heading = rc.getHeading()

def newLeg():
    'Save data, assess position and determine new path'
 
    g.data[1] = rc.getDistanceMade()
    
    debugLog = open (g.debugFilename, 'a')
    debugLog.write(' \n')
    debugLog.write(str(g.data))
    debugLog.close()

    rc.checkBalance()

    legFound = False
    g.lastTurn = 0
    Left = -1
    Right = 1

    while not legFound:
        g.data[2] = rc.getHeading()
        g.data[4:] = rc.getSonarReading()
        print g.data

        if g.data[7] <= g.innerBoundFront:                                       # No room to turn; need to back-out (FF90) 
            backout()

        elif g.data[7] <= g.outerBoundFront:                                     # No room to run and stop (FF90)
            if g.data[4] > g.data[10]:                                           # Check side sensors (SL90 & SR90) 
                makeTurn(Left * 45)
            else:
                makeTurn(Right * 45) 

        elif g.data[6] <= g.outerBoundFront:                                     # Check left front for passage width (FL90)
            if g.lastTurn < 0:                                                   # Prevent reversing last turn
                makeTurn(Left * 45)
            else:
                makeTurn(Right * 45)

        elif g.data[8] <= g.outerBoundFront:                                     # Check right front for passage width (FR90)
            if g.lastTurn > 0:       
                makeTurn(Right * 45) 
            else:
                makeTurn(Left * 45)             

        elif g.data[5] <= g.innerBoundAngle:                                     # Check (a bit) in between front sensors (FL70)
             if g.lastTurn < 0:
                makeTurn(Left * 45) 
             else:
                makeTurn(Right * 45)

        elif g.data[9] <= g.innerBoundAngle:                                     # (FR70)
             if g.lastTurn > 0:            
                makeTurn(Right * 45) 
             else:
                makeTurn(Left * 45)              

        elif g.data[4] <= g.innerBoundSide:                                      # Check if not too close to a wall (SL90)
             makeTurn(Right * 45)
             
        elif g.data[10] <= g.innerBoundSide:                                     # (SR90)
             makeTurn(Left * 45)              

        elif (min(g.data[6:9]) - g.outerBoundFront) <= g.minDist2Move:           # Only start if free space makes sense
            if g.data[4] > g.data[10]:
                makeTurn(Left * 45)
            else:
                makeTurn(Right * 45)
        
        else:
            legFound = True

            g.data[0] = (min(g.data[6:9]) - g.outerBoundFront)                   # Set distanceTarget in meters
            rc.setDistanceTarget(g.data[0])
            
            g.data[1] = 0.0                                                      # Not nescessary

            #if g.data[3] <= g.outerBoundS90:                                    # WIP - to build in: stick-to-one-hand mechanism 
            #    g.wallAtLeft = True
            #if g.data[9] <= g.outerBoundS90:
            #    g.wallAtRight = True

            g.data[2] = rc.getHeading()                                          # g.data[2] = int(round(g.data[2],0))
            g.data[3] = 0
            rc.setSpeed(11)
            print g.data            
            g.runStart = time.clock()                                            # (Re-)start 'watchdog' in case the bot gets stuck somewhere

 
if __name__ == "__main__":
    initTrajectory()

    while g.Operational:
        rbState = rc.getRobotState()
        while rbState != 'F':                                                    # distanceMade < distanceTarget
            g.data[4:] = rc.getSonarReading()                                    # Security status check
            '''
            if min(g.data[6:9]) <= g.innerBoundFront:                            # Unexpected obstacle in front
                rc.setSpeed(0)
                g.data[3] = 1                                                    # Record the event
                rbState = 'F'
            '''
            if g.data[4] <= g.innerBoundSide or g.data[10] <= g.innerBoundSide: # Running towards a wall
                rc.setSpeed(0)
                g.data[3] = 2
                rbState = 'F'
            elif (time.clock() - g.timerStart) >= 0.2:
                rbState = rc.getRobotState()
                print rbState
                g.timerStart = time.clock()    
            elif (time.clock() - g.runStart) > 10:
                if rbState != 'F':
                    speed = rc.getSpeed()
                    if speed > 0.01 or speed < -0.01:
                        g.runStart = time.clock()
                    else:
                        runFailure()
        newLeg()
 