#!/usr/bin/python

import sys
import time
import control as rc
import sonar
import measurements

class globals(object): pass
g = globals()

g.POSITIVE = False
g.RUN = True
g.sonarStart = time.clock()
g.runStart = time.clock()
g.distanceMade = 0.0
g.frontBound = 50
g.sideBound = 25    
g.speedPerc = 0
g.sumTurns = 0
g.bearing = 0.0
g.trajectory = []
g.L90 = 0
g.L70 = 0
g.F90 = 0
g.R70 = 0
g.R90 = 0
g.sin20 = 0.342020 #sinus 20 degrees
g.sin70 = 0.939693 #sinus 70 degrees
g.sin20d70 = 0.36397 #sin20 /sin70 Used for frontal footprint
g.sin70d20 = 2.74747968 #sin70/sin20 Used for parallel footprint
g.parallelRight = False
g.parallelLeft = False
g.setpoint = 0
g.lastDist70 = 0
g.virtualCentreOffset = 13  

#mR = measurements.valueHandler("Sec", "L70", "Frt", "R70")

def mainExit():
    rc.stopMessaging()
    sys.exit()    

def runStopped():
    rc.setDistanceTarget(0)
    print "runTimer expired without an actual speed"
    print "sonar =","\t",g.L90,"\t",g.L70,"\t",g.F90,"\t",g.R70,"\t",g.R90
    print "trajectory =",g.trajectory
    g.RUN = False
    mainExit()
 
def question(string):
    answer = ' '
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
         answer = raw_input(string)
    if answer == 'j' or answer == 'J':
        g.POSITIVE = True
    else:
        g.POSITIVE = False

def sonarTimer(sec):
    timer = time.clock() - g.sonarStart
    while timer < sec:
        timer = time.clock() - g.sonarStart
    g.sonarStart = time.clock()

def initRB():
    print ("state RB = Initializing")
    rbState = rc.getRobotState()
    if rbState == 'D':
        print "RB status = Horizontal"
    while rbState != 'U':
        rbState = rc.getRobotState()
    print "RB status = Vertical"
    g.RUN = True

    rc.checkBalance()
   
def turnRB():
    turnAngle = input("Degrees to run [pos/neg]     ? ")
    rc.setTurn(turnAngle)
    rc.checkBalance()

def waitWhileTurning():
    'Timer loop. Only executed when there is actualy a turn to make' 

    gap = rc.getDeclination()
    while gap > 1.0 or gap < -1.0:
        gap = rc.getDeclination()

def newSection():
    rc.setDistanceTarget(0.0)
    # checkBalance()                                            # Optional
    g.parallelLeft = False
    g.parallelRight = False
    g.setpoint = 0
    g.sumTurns = 0
    sectionFacts = [g.bearing, g.distanceMade]
    g.trajectory.append(sectionFacts)                           # For future use like reversed trajectory         
    g.distanceMade = 0.0
    g.bearing = rc.getHeading
        
    g.L90, g.L70, g.F90, g.R70, g.R90 = sonar.readSensors()
    
    if g.F90 > g.frontBound:                                    # Add an action for minimum bounds; bot has to backup first then
        forwardDistance = g.F90 / 100.0
        print forwardDistance      
        rc.setDistanceTarget(forwardDistance)                   # There's at least 0.51m to move
        print "Distance set"
        if g.R90 <= g.sideBound:                                # Check if there's a wall alongside
            g.parallelRight = True
            g.setpoint = ((g.R90+6)/g.sin20)-7                  # Target distance to read for the sensor at 70 degrees when exactly parallel
        elif g.L90<= g.sideBound:
            g.parallelLeft = True
            g.setpoint = ((g.L90+6)/g.sin20)-7
        rc.setSpeed(10)
        print "Speed set"
        time.sleep(10)
        g.runStart = time.clock()                               # Start a 'watchdog' in case the bot gets stuck somewhere

def stayParallel(dist):
    compensate = 0
    P = (dist - g.setPoint) / 100                               # 
    D = (dist - g.lastDist70) / 100                             # 
    g.lastDist70 =  dist
    compensate  = P - D

if __name__ == "__main__":
    initRB()                                                    # Check for vertical position and balance

    question("Start test?")                                     # Yes or even no will do ;-)

    g.bearing = rc.getHeading()                                 # Keep the starting direction

    newSection()

    while g.RUN:
        #sonarTimer(0.01)                                        # To avoid sensor overload (takes < 1 cm distance)
        g.L90, g.L70, g.F90, g.R70, g.R90 = sonar.readSensors() 
        
        if g.F90 <= g.frontBound:                                # Bot might be too close to an obstacle
            if (g.L90 + g.L70) >= (g.R90 + g.L70):               # Check for the largest room to manouvre
                angle = -90                                      # Needs update for approaching a corner at 45 degrees 
            else:
                angle = 90
            rc.setTurn(angle)
            rc.waitWhileTurning()
            g.distanceMade = rc.getDistanceMade()
            newSection()
        else:
            forwardDistance = g.F90 / 100.0
            rc.setDistanceTarget(forwardDistance)
            rc.setSpeed(10)
            angle = 0
            if g.parallelRight:
                angle = stayParallel(g.R70)
                g.sumTurns += angle
                if abs(g.sumTurns) < 360:
                    rc.setTurn(angle)
                else:
                    g.parallelRight =  False
                    g.distanceMade = rc.getDistanceMade()            
                    newSection()
            elif g.parallelLeft:
                angle = -stayParallel(g.L70)
                g.sumTurns += angle
                if abs(g.sumTurns) < 360:
                    rc.setTurn(angle)
                else:
                    g.parallelLeft =  False
                    g.distanceMade = rc.getDistanceMade()
                    newSection()
        '''
        if (time.clock() - g.runStart) > 10:
            speed = rc.getSpeed()
            if speed > 0.0:
                g.runStart = time.clock()
            else:
                runStopped()
        '''
