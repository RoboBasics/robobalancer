##########################################################################
# This script will handle: 
# - transmissions to/from Arduino Uno
# - the (distance) sensors
# - the Pi camera
#
# All set-functions use the commSync for synchronization with the Arduino control loop (0.01 sec) 
#
# Command Methods #########################################################
# setSpeed(targetSpeed)             - input in integer percentage (neg/pos)
# setDistanceTarget(targetDistance) - input in meters (pos)
# setTurn (targetTurn)              - input in degrees (neg/pos)
#
# Monitor Methods #########################################################
# getSpeed()                        - returns actual speed in m/s
# getDistanceMade()                 - returns distance made in m
# getHeading()                      - returns target heading in degrees
# checkBalance()                    - forces wait until speed < 0.01 m/s
# getRobotState()                   - returns Up or Down
# getRanges()                       - returns 8x ranges in mm
# getMedianRanges()                 - returns 8x median out of 3 ranges
# getThrottle()                     - returns the target distance
#
# Tuning Methods ##########################################################
# setPowerGainVert (KpPitch)
# setDampGainVert (KdPitch)
# setPowerGainHor (KpHor)
# setDampGainHor (KdHor)
# getPowerGainVert()
# getDampGainVert()
# getPowerGainHor()
# getDampGainHor()
# getTargetVert()
# getTargetHor()
# getErrorVert()
# getErrorHor()
# 
# Logging methods #########################################################
# getStatusStartup()
# getStatusPort()
# getStatusThread()
# getStatusArduinoConnection()
##########################################################################

#!/usr/bin/python

import time
import math
import linkingStorage as ls
import messaging
import sonar
#import measurements

commSyncStart = time.clock()

def commSync():
    'Timed delay to synchronize actions with Arduino control loop'
    global commSyncStart

    while time.clock() - commSyncStart < 0.01:
        pass
    commSyncStart = time.clock()

def setSpeed(targetSpeed):
    ls.speedTarget = testInteger(targetSpeed)
    ls.speedTarget = constrain(ls.speedTarget, -100, 100) 
    while ls.SEND is True:
        continue
    ls.sendCode = 'S'
    ls.SEND = True       # force a 'wait-for-completion' upon transmissions
    commSync()

def getSpeed():
    while ls.SEND is True:
        continue
    ls.sendCode = 's'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.speedActual

def setDistanceTarget(targetDistance):
    ls.distanceTarget = testFloat(targetDistance)
    ls.distanceTarget = constrain(ls.distanceTarget, 0.0, 100.0) 
    while ls.SEND is True:
        continue
    ls.sendCode = 'D'
    ls.SEND = True
    commSync()

def getDistanceMade():
    while ls.SEND is True:
        continue
    ls.sendCode = 'd'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.distanceMade

def setTurn (targetTurn):
    ls.turnTarget = testInteger(targetTurn)
    ls.turnTarget = constrain(ls.turnTarget, -180, 180)
    while ls.SEND is True:
        continue
    ls.sendCode = 'T'
    ls.SEND = True
    commSync()

def getHeading():
    while ls.SEND is True:
        continue
    ls.sendCode = 'h'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:                # a bit tricky: blocks until any(!) message is recieved
        continue
    return ls.heading   

def getDeclination():
    while ls.SEND is True:
        continue
    ls.sendCode = 'y'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.declination

def avoidObstacles():

    if ls.speedTarget != 0:
        if ls.distanceTarget > 0:
            getDistance()
            if ls.distanceMade != 0.0:
                if ls.reboundAngle != 0:
                    adjustHeading()
                else:
                    ls.distanceTarget -= ls.distanceMade
            getMedianRanges()
            #print ls.usM
            for t in range (8):
                if ls.usM[t] <= ls.minBoundary[t]:
                    setSpeed(0)
                    return
            checkForObstacles()
        else:
            setSpeed(0)

def adjustHeading():
    ######################################################################################
    # Calculate angle and distance from current position to the target position
    # If obstacles are found the angle will be overwritten by the rebound angle
    # Input: actual speed, target distance
    # Output: angle to turn, new target distance
    ######################################################################################
 
    # Take the last reboundAngle in radians as angle (will be zero if no rebound occured)
    angle_A = math.radians(ls.reboundAngle)
        
    # Get the distance made in this interval 
    side_b = ls.distanceMade
    side_c = ls.distanceTarget
        
    # New adjusted distance by the rule a2=b2+c2-2bccosA
    side_b2 = pow(side_b,2)
    side_c2 = pow(side_c, 2)
    side_a2 = (side_b2 + side_c2)- 2 * side_b * side_c * math.cos(angle_A)
    side_a = sqrt(side_a2)
    setDistanceTarget(side_a)

     # cosY = (a2+b2-c2)/(2ab)
    angle_Ycos = (side_a2+side_b2-side_c2) / (2 * side_a * side_b)
    angle_Y = math.acos(angle_Ycos)
    angle_Y = math.degrees(angle_Y)

    # Angle to turn is the complementairy angle
    turnAngle = 180 - angle_Y
    # Adjust for evading direction
    if ls.reboundAngle > 0.0:
        turnAngle *= -1
    turnAngle = int(turnAngle)
    setTurn(turnAngle)

    m.keepValues(angle_A, side_b, side_a, turnAngle)

    ls.reboundAngle = 0.0

def checkForObstacles():
    reboundToRight = 0
    reboundToLeft = 0
    for t in range (8):
        if ls.usM[t] < 4000:
            if ls.usM[t] <= ls.boundary[t]:
                ls.reboundAngle += (1 - ls.usM[t]/ls.boundary[t]) * ls.maxReboundAngle[t]
                if ls.reboundAngle > reboundToRight:
                    reboundToRight = ls.reboundAngle
                elif ls.reboundAngle < reboundToLeft:
                    reboundToLeft = ls.reboundAngle
    if reboundToRight + reboundToLeft > 0.0:
        ls.reboundAngle = reboundToRight
    else:
        ls.reboundAngle = reboundToLeft

def getRobotState():
    while ls.SEND is True:
        continue
    ls.sendCode = 'k'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.robotState

def getSonarReading():
    ls.newReading = False
    sonar.readSensors()
    while not ls.newReading:
        pass 
    return ls.sonarValues

def getFrontSonar():
    ls.newReading = False
    sonar.readFrontSensors()
    while not ls.newReading:
        pass 
    return ls.sonarValues[2:5]

def checkBalanceRavg(samples):
    'Checks the actual speed and calculates a running average Uses commSync (Speed only changes within Arduino control loop)'
    speedAvg = [0.0] * samples
    for t in range(samples):
        speedAvg[t] = getSpeed()
        commSync()                                             
    runAvg = float(sum(speedAvg) / len(speedAvg))

    t = 0 
    while not -0.01 <= runAvg <= 0.01:
        speedAvg[t] = getSpeed()
        commSync()
        runAvg = float(sum(speedAvg) / len(speedAvg))
        t += 1
        if t > (samples-1):
            t = 0
    
    print("Balancing")
    #print(" (Average speed = "),
    #print (runAvg),
    #print (")")

def checkBalance():
    balancing = 0
    while balancing < 3:
        commSync()
        speed = getSpeed()
        if -0.01 <= speed <= 0.01:
            balancing += 1
    print("Balancing")

def waitWhileTurning():
    'Timer. Only executed if bot is actualy turning' 
    gap = getDeclination()
    while gap > 0.5 or gap < -0.5:
        commSync()
        gap = getDeclination()


def getStatusStartup():
    port = getStatusPort()
    thread = getStatusThread()
    connect = getStatusArduinoConnection()
    return port, thread, connect

def getStatusPort():
    return ls.commPortOpened

def getStatusThread():
    return ls.commThreadStarted

def getStatusArduinoConnection():
    return ls.CONNECTION

def setPowerGainVert (KpPitch):
    ls.KpPitch = testFloat(KpPitch)
    ls.KpPitch = constrain(ls.KpPitch, 0.0, 50.0)
    while ls.SEND is True:
        continue
    ls.sendCode = 'A'
    ls.SEND = True
    commSync()

def getPowerGainVert():
    while ls.SEND is True:
        continue
    ls.sendCode = 'a'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.KpPitch

def setDampGainVert (KdPitch):
    ls.KdPitch = testFloat(KdPitch)
    ls.KdPitch = constrain(ls.KdPitch, 0.0, 50.0)
    while ls.SEND is True:
        continue
    ls.sendCode = 'B'
    ls.SEND = True
    commSync()

def getDampGainVert():
    while ls.SEND is True:
        continue
    ls.sendCode = 'b'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.KdPitch

def setPowerGainHor (KpHor):
    ls.KpHor = testFloat(KpHor)
    ls.KpHor = constrain(ls.KpHor, 0.0, 50.0)
    while ls.SEND is True:
        continue
    ls.sendCode = 'E'
    ls.SEND = True
    commSync()

def getPowerGainHor():
    while ls.SEND is True:
        continue
    ls.sendCode = 'e'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.KpHor

def setDampGainHor (KdHor):
    ls.KdHor = testFloat(KdHor)
    ls.KdHor = constrain(ls.KdHor, 0.0, 50.0)
    while ls.SEND is True:
        continue
    ls.sendCode = 'F'
    ls.SEND = True
    commSync()

def getDampGainHor():
    while ls.SEND is True:
        continue
    ls.sendCode = 'f'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.KdHor

def getTargetVert():
    while ls.SEND is True:
        continue
    ls.sendCode = 'g'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.pitchTarget

def getTargetHor():
    while ls.SEND is True:
        continue
    ls.sendCode = 'm'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.tickTarget

def getErrorVert():
    while ls.SEND is True:
        continue
    ls.sendCode = 'j'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.pitchError

def getErrorHor():
    while ls.SEND is True:
        continue
    ls.sendCode = 'l'
    ls.SEND = True
    ls.RECIEVED = False
    while ls.RECIEVED is False:
        continue
    return ls.tickError

def stopThreading():
    while ls.SEND is True:
        continue
    ls.sendCode = 'X'
    ls.SEND = True
    ls.commThreadStarted = False
    ls.sonarThreadStarted = False
    
def testInteger(value):
    try:
        test = int(value)
        return test
    except ValueError:
        return 0

def testFloat(value):
    try:
        test = float(value)
        return test
    except ValueError:
        return 0.0

def constrain(val, min_val, max_val):
    return min(max_val, max(min_val, val))

def outputMeasurements():
    m.writeValues()
    m.printValues()


