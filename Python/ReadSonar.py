#!/usr/bin/python

import time
import sonar


class globals(object): pass
g = globals()

g.POSITIVE = False
g.data = [0] * 10

def question(string):
    answer = ' '
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
         answer = raw_input(string)
    if answer == 'j' or answer == 'J':
        g.POSITIVE = True
    else:
        g.POSITIVE = False

if __name__ == "__main__":
    question("Start?")
    while g.POSITIVE:
        for t in range(100):
            g.data[0] = t
            g.data[3:] = sonar.readSensors()
            print g.data
        question("New test?")    
    



 