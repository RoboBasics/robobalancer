##########################################################################
# Alternative vertical gains
##########################################################################
#rc.setPowerGainVert(1.129321181)            # Default
#rc.setDampGainVert(0.05049)
#rc.setPowerGainVert(1.106197329)            # Alternative 1
#rc.setDampGainVert(0.05049)
#rc.setPowerGainVert(0.956214364)            # Alternative 2
#rc.setDampGainVert(0.054345071)
#rc.setPowerGainVert(0.935189245)            # Alternative 3
#rc.setDampGainVert(0.04049)
#rc.setPowerGainVert(0.844053849)            # Before US sensors were installed
#rc.setDampGainVert(0.040492132)
##########################################################################

#!/usr/bin/python
#import threading
import time
import control as rc
import measurements

m = measurements.valueHandler('Run','Power','Damp', 'Best')

class struct(object): pass
g = struct()

g.gain = [0.0, 0.0]
g.gainSave = [0.0, 0.0]
g.step = [0.1, 0.01]
g.tolerance = 0.00001
g.bestError = 0.0

STOP = False
POSITIVE = False

def question(string):
    global POSITIVE
    answer = ' '
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
         answer = raw_input(string)
    if answer == 'j' or answer == 'J':
        POSITIVE = True
    else:
        POSITIVE = False

def getValue(string):
    answer = raw_input(string)
    return answer


def getError():
    rc.setPowerGainVert(g.gain[0])
    rc.setDampGainVert(g.gain[1])
    error = [0.0] * 1
    for t in range (len(error)):
        delta = rc.getErrorVert()
        error[t] = abs(delta)
    errorAvg = sum(error) / len(error)
    return errorAvg

if __name__ == "__main__":

    while STOP is False:
        g.gain[0] = rc.getPowerGainVert()
        g.gainSave[0] = g.gain[0]
        g.gain[1] = rc.getDampGainVert()
        g.gainSave[1] = g.gain[1]
        print g.gain[0],
        print g.gain[1]
        
        rbState = rc.getRobotState()
        if rbState == 'D':
            print "robot position is horizontal"
        while rbState != 'U':
            rbState = rc.getRobotState()
        print "robot position is vertical"
        POSITIVE = False
        while not POSITIVE:
            question("Steady balancing? ")

        g.bestError = getError()
        n = 0
        while sum(g.step) > g.tolerance:
            for t in range (len(g.gain)):
                g.gain[t] += g.step[t]
                error = getError()
                if error < g.bestError:
                    g.bestError = error
                    g.step[t] *= 1.1 
                else:
                    g.gain[t] -= 2 * g.step[t] 
                    error = getError()
                    if error < g.bestError:
                        g.bestError = error
                        g.step[t] *= 1.1
                    else:
                         g.gain[t] += g.step[t]
                         g.step[t] *= 0.9
            n += 1
            m.keepValues(n, g.gain[0], g.gain[1], g.bestError)
        m.writeValues()
        m.printValues()

        question("Another Twiddle? [j/n]")
        if POSITIVE:
            question("Same starting point? [j/n] ")
            if POSITIVE:
                rc.setPowerGainVert(g.gainSave[0])
                rc.setDampGainVert(g.gainSave[1])
            g.step = [0.1, 0.01]
            continue
        else:
            STOP = True
    print "Ready"

