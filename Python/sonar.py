####################################
# sensor 1   Board  socket  Function
#    1         2      3       SL90
#    2         1      3       FL70
#    3         1      4       FL90    
#    4         1      1       FF90
#    5         2      2       FR90 
#    6         1      2       FR70
#    7         2      4       SR90  
####################################

#!/usr/bin/python

import time
import UltraBorg

class UltraSonic:

    def __init__(self):
        # Start UltraBorg board #1, address 10
        self.UB1 = UltraBorg.UltraBorg()
        self.UB1.i2cAddress = 10
        self.UB1.Init()
        # Start UltraBorg board #2, address 11
        self.UB2 = UltraBorg.UltraBorg()
        self.UB2.i2cAddress = 11
        self.UB2.Init()
        self.sonarReadings = [0.0] * 7
        self.timer = time.clock()

    def getSonarReading(self):

        while time.clock() - self.timer <= 0.02:
            pass
        self.timer = time.clock()

        self.usm = self.UB2.GetRawDistance3()                        # SL90 (2;3)
        while self.usm == 0:
            self.usm = self.UB2.GetRawDistance3()
        self.sonarReadings[0] = round((self.usm/1000.0),2) 

        self.usm = self.UB1.GetRawDistance3()                        # FL70 (1;3)
        while self.usm == 0:
            self.usm = self.UB1.GetRawDistance3()
        self.sonarReadings[1] = round((self.usm/1000.0),2) 

        self.usm = self.UB1.GetRawDistance4()                        # FL90 (1;4)
        while self.usm == 0:
            self.usm = self.UB1.GetRawDistance4()
        self.sonarReadings[2] = round((self.usm/1000.0),2) 

        self.usm = self.UB1.GetRawDistance1()                        # FF90 (1;1)
        while self.usm == 0:
            self.usm = self.UB1.GetRawDistance1() 
        self.sonarReadings[3] = round((self.usm/1000.0),2)

        self.usm = self.UB2.GetRawDistance2()                        # FR90 (2;2)
        while self.usm == 0:
            self.usm = self.UB1.GetRawDistance2()
        self.sonarReadings[4] = round((self.usm/1000.0),2) 

        self.usm = self.UB1.GetRawDistance2()                        # FR70 (1;2)
        while self.usm == 0:
            self.usm = UB1.GetDistanc2()
        self.sonarReadings[5] = round((self.usm/1000.0),2) 
 
        self.usm = self.UB2.GetRawDistance4()                        # SR90 (2;4)
        while self.usm == 0:
            self.usm = self.UB2.GetRawDistance4()
        self.sonarReadings[6] = round((self.usm/1000.0),2)
        
        return self.sonarReadings
