#!/usr/bin/python

import time

# command variables
turnTarget = 0                                                          # flag = T
speedTarget = 0                                                         # flag = S
distanceTarget = 0.0                                                    # flag = D

# status variables
heading = 0.0                                                           # flag = h (is same as yawTarget)
declination = 0.0                                                       # flag = y (is same as yawError)
speedActual = 0.0                                                       # flag = s
distanceMade = 0.0                                                      # flag = d
robotState = ' '                                                        # flag = r

# tuning variables                                                      # flags for Send & Recieve
KpPitch = 0.0                                                           # flags = A, a
KdPitch = 0.0                                                           # flags = B, b
KpHor = 0.0                                                             # flags = E, e
KdHor = 0.0                                                             # flags = F, f
pitchTarget = 0.0                                                       # flags = g
pitchError = 0.0                                                        # flags = j
tickError = 0.0                                                         # flags = l
tickTarget = 0.0                                                        # flags = m

# flow variables and switches
sendCode = ' '                                                           # variable for sending and recieving flags
marker = '\n'                                                            # mark message ending

commPortOpened = False                                                   # start-uo flag
commThreadStarted = False                                                # start-up flag
CONNECTION = False                                                       # flag for handschake
RECIEVED = False                                                         # switch for new message revieved
SEND = False                                                             # switch for new request send

# Obstacle avoiding variables
reboundAngles = [ 10, 45, 90, -45, -10]                                 # used for relative rebound
sensorWeight = [5, 5, 55, 5, 5]                                         # used to create a significant sensitivity bubble (boundary)
minBoundary = [ 70, 100, 200, 250, 250, 200, 100, 70]                   # used to detect an emergency 
reboundAngle = 0.0                                                      

testCounter = 0

