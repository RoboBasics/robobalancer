#!/usr/bin/python

import time
import csv

csv.register_dialect('excel', delimiter=';', lineterminator='\n')

class valueHandler:
    def __init__(self, *args):
        self.measurements = []
        self.keys = []
        for t in range (len(args)):
            self.keys.append(args[t])

    def keepValues(self, *args):
        values = {}
        for t in range(len(self.keys)):
            values[ self.keys[t] ] = args[t]
        self.measurements.append(values)

    def printValues(self):
        for t in range(len(self.keys)):
            print self.keys[t],
            print "\t",
        print " "
        for t in range(len(self.measurements)):
            for x in range (len(self.keys)):
                print(self.measurements[t][self.keys[x]]),
                print "\t",
            print " "

    def writeValues(self):
        outputFilename = "Measurements_{0}.csv".format( int( time.time() ) )
        with open( outputFilename, "w" ) as csvFile:
            dictWriter = csv.DictWriter( csvFile, self.keys)
            dictWriter.writeheader()
            dictWriter.writerows( self.measurements )
