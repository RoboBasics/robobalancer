#!/usr/bin/python
# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
from matplotlib import pyplot as plt

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (480, 360)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size(480,360))

# allow the camera to warmup
time.sleep(0.1)

def grabImage():
    # grab an image from the camera
    camera.capture(rawCapture, format="bgr")
    image = rawCapture.array
    canny_image = cv2.Canny(image,threshold1=90, threshold2=190)
    return image, canny_image
    
def displayImage(image, canny_image):
    # display the image on screen and wait for a keypress
    cv2.imshow("Image", image)
    cv2.imshow("Canny Image", canny_image)
    #cv2.waitKey(0)
    rawCapture.truncate(0)

'''
while True:

    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        picture = frame.array
        cv2.imshow("Frame", picture)
        key = cv2.waitKey(1) & 0xFF
        rawCapture.truncate(0)
        if key == ord("q"):
            break
'''    
    
    
    
image, canny_image = grabImage()
plt.imshow(image, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.show()
        
#    displayImage(image, canny_image)
#    key = cv2.waitKey(1) & 0xFF
#    if key == ord("q"):
#        break
    



'''
import sys
import time
import camera_streamer
import numpy as np
import cv2

class globals(object): pass
g = globals()

g.POSITIVE = False
g.RUN = True
g.sonarStart = time.clock()
g.runStart = time.clock()
g.distanceMade = 0.0
g.frontBound = 50
g.sideBound = 25    
g.speedPerc = 0
g.sumTurns = 0
g.bearing = 0.0
g.trajectory = []
g.L90 = 0
g.L70 = 0
g.F90 = 0
g.R70 = 0
g.R90 = 0
g.sin20 = 0.342020 #sinus 20 degrees
g.sin70 = 0.939693 #sinus 70 degrees
g.sin20d70 = 0.36397 #sin20 /sin70 Used for frontal footprint
g.sin70d20 = 2.74747968 #sin70/sin20 Used for parallel footprint
g.parallelRight = False
g.parallelLeft = False
g.setpoint = 0
g.lastDist70 = 0
g.virtualCentreOffset = 13  

cameraStreamer = camera_streamer.CameraStreamer()
#cameraStreamer.startStreaming()

cap = cv2.VideoCapture(0)

def mainExit():
    sys.exit()    

def question(string):
    answer = ' '
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
         answer = raw_input(string)
    if answer == 'j' or answer == 'J':
        g.POSITIVE = True
    else:
        g.POSITIVE = False


if __name__ == "__main__":
    while(True):
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Our operations on the frame come here
        #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Display the resulting frame
        #cv2.imshow('frame',gray)
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
    
    question("Stop streaming?")
    if g.POSITIVE:
        cameraStreamer.stopStreaming()    
    
'''
