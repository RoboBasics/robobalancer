##########################################################################
# This is the main script to program the balancer
# Include control.py to connect to the Arduino, camera and sensors 
##########################################################################

#!/usr/bin/python

import time
import control as rc
import measurements

#interval, target, error, distance

STOP = False
POSITIVE = False

def question(string):
    global POSITIVE
    answer = ' '
    while answer != 'j' and answer != 'J' and answer != 'n' and answer != 'N':
         answer = raw_input(string)
    if answer == 'j' or answer == 'J':
        POSITIVE = True
    else:
        POSITIVE = False

def getValue(string):
    answer = raw_input(string)
    return answer

def testRun(distanceTarget):
    m = measurements.valueHandler('Target','Error','Dist')

    global POSITIVE
    gainP = rc.getPowerGainHor()
    gainD = rc.getDampGainHor()
    print "Start values: ",
    print (gainP),
    print (gainD),
    print (distanceTarget)

    rbState = rc.getRobotState()
    if rbState == 'D':
        print "robot position is horizontal"
    while rbState != 'U':
        rbState = rc.getRobotState()
    print "robot position is vertical"

    POSITIVE = False
    while not POSITIVE:
        question("Steady balancing? ")

    target = rc.getTargetHor()
    error = rc.getErrorHor()
    distance = rc.getDistance()
    print "Before target setting = ",
    print "Target = ",
    print (target),
    print "Error = ",
    print (error),
    print "Distance = ",
    print (distance)

    rc.setDistance(distanceTarget)

    target = rc.getTargetHor()
    error = rc.getErrorHor()
    distance = rc.getDistance()
    print "After target setting = ",
    print "Target = ",
    print (target),
    print "Error = ",
    print (error),
    print "Distance = ",
    print (distance)

    timeLast = time.time()

    while distance < distanceTarget:
        #timeNow = time.time() - timeLast
        #interval = round(timeNow,2)
        target = rc.getTargetHor()
        error = rc.getErrorHor()
        distance = rc.getDistance()
        m.keepValues(target, error, distance)            
    m.writeValues()
    m.printValues()

def menu():
    global STOP, POSITIVE

    question("Run ? ")
    if POSITIVE:
        distanceTarget = input("Run test for how many meters? ")
        pGain = rc.getPowerGainHor()
        print "Current kP value = ",
        print (pGain)
        question("Change P-gain? ")
        if POSITIVE:
            pGain = input("New kP value: ")
            rc.setPowerGainHor(pGain)
        dGain = rc.getDampGainHor()
        print "Current kD value = ",
        print (dGain)
        question("Change D-gain? ")
        if POSITIVE:
            dGain = input("New kD value: ")
            rc.setDampGainHor(dGain)
        question("Turn the robot? ")
        if POSITIVE:
            rc.setTurn(180)
        testRun(distanceTarget)
    else:
        STOP = True

if __name__ == "__main__":
    while STOP is False:
        menu()
    print "Finished"

