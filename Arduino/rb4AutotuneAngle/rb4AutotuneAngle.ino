/*==================================================================================
This script operates as 'firmware' for a RPi controlled balancer. The complete 
sketch also handles serial communication over usb with the Rpi

Functions:
- IMU handling (gyroscope & accelerometer. Magnetometer heading will be  
  added in a future version )
- Encoder handling
- Motor handling (balancing, forward, backward and turning)
- Bluetooth communication for gain tuning and motion alternative control
- USB serial communication for command & status updates 

Hardware used:
- Pololu High Power DC motors 210 rpm, 47:1, 80 oz-in
- Hall encoders 2248.8 cpr (48 cpr after gear )
- Pololu wheels 90  x 10 mm
- Arduino UNO
- Seeed Studio 4A motor shield
- Dagu bluetooth module

The motor shield uses pin 9 & 10 as PWM pins, the encoders use pin 3 and 4 for 
interrupts. So the sketch cannot handle servo's  
(servo.h = timer1/D9+10, servoTimer2.h = timer2/D3+11)

The servo's of the robot camera and IR-radar will be controlled by Rpi through
an Ultraborg board.

Binary sketch size: 23.032 bytes (used 71% of a 32.256 byte maximum) (0,84 secs)
Minimum Memory Usage: 1086 bytes (53% of a 2048 byte maximum)

====================================================================================*/

#include <Encoder.h>
#include <math.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>
#include <MovingAverageFilter.h>
#include <TimedAction.h>
#include <SoftwareSerial.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include "I2Cdev.h"

/* Pinsettings =====================================================================

D0 = ---- (Rx)         D6 = motor IN2        D12 = Blue led     A0 = bluetooth RX
D1 = ---- (Tx)         D7 = motor IN3        D13 = ---- (led)   A1 = bluetooth TX
D2 = Right encoder A   D8 = motor IN4                           A2 = ----
D3 = Left  encoder A   D9 = motor PWM A      SDA = IMU          A3 = ----
D4 = Right encoder B   D10 = motor PWM B     SCL = IMU          A4 = ----
D5 = motor IN1         D11 = Left encoder B                     A5 = ----

=====================================================================================*/

const unsigned int MOTOR_PIN1 = 5;
const unsigned int MOTOR_PIN2 = 6;
const unsigned int MOTOR_PIN3 = 7;
const unsigned int MOTOR_PIN4 = 8;
const unsigned int SPEEDPIN_A = 9;
const unsigned int SPEEDPIN_B = 10;
const unsigned int LEDPIN     = 12;          // Blue led on top of the robo)

// Motors ===========================================================================

const double maxTickSec  = 7870.800;         // RPM(210) / 60 * ticks/revolution (2248.800 || 48) 
const double tickMeter   = 7953.503;         // Ticks/revolution * wheel perimeter (0.2827433)
const double tickPwm     = 30.86588;         // maxTickSec/255
const double tickRadian  = 357.9076;         // ticks per revolution / 2*Pi

double speedPerc, tickTarget, boostLevel, boost, boostStep;
unsigned int pwm;

// Encoders =========================================================================

Encoder encMotorRight(2, 4);
Encoder encMotorLeft(3, 11);

int encValLeft, encValRight;
double tickSpeedLeft, tickSpeedRight, encValLeftLast, encValRightLast;

/* IMU settings =====================================================================
The Digital Motion Processor of the MPU6050 is used:
- Because it's there 
- Saves the effort of coding the angle and filter calculations
- Saves CPU/memory of the Arduino
- Performs self calibration
Downside: the self calibration requires a substantial initiation time 
=====================================================================================*/

MPU6050 imu;
I2Cdev   I2C_M;

bool dmpReady = false;  // set true if DMP init was successful

uint8_t devStatus;      // return status after each DMP operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

const double deadAngle = 0.7854;            // 45degr in radians
double pitch, angle;

MovingAverageFilter movingAverageAngle(15); // Filter for pitch angle
//MovingAverageFilter movingAverageSpeed(20); // Filter for encoder values

// Messaging ========================================================================

SoftwareSerial Bluetooth(A0, A1); // RX, TX

// Timing ===========================================================================

TimedAction ControlAngle     = TimedAction(20, controlAngle);
TimedAction ControlSpeed     = TimedAction(40, controlSpeed);

const double timedInterval = 0.02;

/* Control loop cascade ============================================================

Outer loop = PidSpeed        (0.04s)         Inner loop = PidAngle    (0.02s)
------------------------------------         --------------------------------
target = percentual speed in tick/s
input  = ticks made by encoders
output = speed compensating ticks      =>    target  = speed compensating ticks
                                             input   = linear speed in ticks from IMU                 
                                             output  = motor adjustment ticks

=====================================================================================*/

double tickSpeed, tickComp, tickMotor, tickAngular;

PID PidSpeed  (&tickSpeed,   &tickComp,  &tickTarget,   0, 0, 0, DIRECT);
PID PidAngle  (&tickAngular, &tickMotor, &tickComp,     0, 0, 0, DIRECT);

double KpAngle = 0.30; double KpSpeed = 0.00;
double KiAngle = 0.00; double KiSpeed = 0.00;
double KdAngle = 0.00; double KdSpeed = 0.00;

PID_ATune aTuneAngle(&tickAngular, &tickMotor);

// Status ===========================================================================

boolean TUNING = true;
boolean BACKWARD = false;
boolean CONTROL = false;
boolean UPDATE = false;
boolean MESSAGE = false;
boolean ledOFF = true;

void setup()
{
	Wire.begin();
	Serial.begin(115200); while (!Serial) {}
	Bluetooth.begin(9600); 

	imu.initialize();
	delay(1000);
	Serial.println(imu.testConnection() ? "Connected" : "FAILED !!");
	devStatus = imu.dmpInitialize();
	if (devStatus == 0)
	{
		imu.setDMPEnabled(true);
		dmpReady = true;
		packetSize = imu.dmpGetFIFOPacketSize();  // get expected DMP packet size for later comparison
	}
	else
	{
		Serial.print(F("DMP Initialization failed (code "));
		Serial.print(devStatus);
		Serial.println(F(")"));
	}

	pinMode(MOTOR_PIN1, OUTPUT);
	pinMode(MOTOR_PIN2, OUTPUT);
	pinMode(MOTOR_PIN3, OUTPUT);
	pinMode(MOTOR_PIN4, OUTPUT);
	pinMode(SPEEDPIN_A, OUTPUT);
	pinMode(SPEEDPIN_B, OUTPUT);
	
	pinMode(LEDPIN, OUTPUT);

	PidSpeed.SetSampleTime    (40);
	PidSpeed.SetOutputLimits  (-maxTickSec, maxTickSec);
	PidSpeed.SetTunings       (KpSpeed, KiSpeed, KdSpeed);

	PidAngle.SetSampleTime    (20);
	PidAngle.SetOutputLimits  (-maxTickSec * 0.5, maxTickSec * 0.5);
	PidAngle.SetTunings       (KpAngle, KiAngle, KdAngle);

	//tickMotor = aTuneStartValue;        // tickMotor already starts at zero
	aTuneAngle.SetNoiseBand(80.5292);     // 0.0045 Radian
	aTuneAngle.SetOutputStep(53.6861454); // 0.3 x 0.01 Radian
	aTuneAngle.SetLookbackSec(1);
	aTuneAngle.SetControlType(1);
	
	stop();
	ledLight();

	boostLevel = 2.5;
	boostStep = (boostLevel - 1) / 2;

	Serial.println("Waiting for IMU calibration");
	for (int x = 0; x < 15; x++) { getAngle(); } 
	while (angle != pitch)   { getAngle(); }       // For calibrating the DMP output
	Serial.println("Done");
	blink();
	
	initRobot();
}

void loop()
{
	if (angle <-deadAngle || pitch > deadAngle) { resetRobot(); }
	getMessage();
	getAngle();
	getEncoders();
	ControlAngle.check();      // Perform PidAngle and set CONTROL
	if (CONTROL)
	{
		controlDirection();    
		computeSpeed();
		controlPower();        // Compute PWM value
		throttle();            // Rotate motors
		CONTROL = false;       // Force wait for next timed action

		Serial.print(tickAngular); tab();
		Serial.print(tickMotor); tab();
		Serial.println(pwm); 
	}
	ControlSpeed.check();
}

/* =================================================================================
Control routines
==================================================================================== */

void setTickTarget()
{
	speedPerc = constrain(speedPerc, 0, 100);
	tickTarget = speedPerc / 100.00 * maxTickSec;
	encValLeftLast = 0; encValRightLast = 0;
}

void controlAngle()
{
	tickAngular = pitch * tickRadian / timedInterval;                         // = ticks/sec 
	if(TUNING)
  {
    byte val = (aTuneAngle.Runtime());
    if (val!=0)
    {
      TUNING = false;
    }
    if(!TUNING)
    { //we're done, set the tuning parameters
      KpAngle = aTuneAngle.GetKp();
      KiAngle = aTuneAngle.GetKi();
      KdAngle = aTuneAngle.GetKd();
      PidAngle.SetTunings(KpAngle, KiAngle, KdAngle);
      Serial.println("Tuned gains (Kp/Ki/Kd):");
      Serial.print(KpAngle); tab();
      Serial.print(KiAngle); tab();
      Serial.println(KdAngle);
    }
  }
  else 
  {
    PidAngle.Compute();
  }
  CONTROL = true;
}

void controlSpeed()
{
	//PidSpeed.Compute();
}

void computeSpeed()
{
	tickSpeedLeft   = ((double)encValLeft  - encValLeftLast)  / timedInterval;    // current speed in ticks/sec
	tickSpeedRight  = ((double)encValRight - encValRightLast) / timedInterval;
	
	tickSpeed = (tickSpeedLeft + tickSpeedRight) / 2;
	
	encValLeftLast  = encValLeft;
	encValRightLast = encValRight;
}

void controlDirection()
{
	if (tickMotor >= 0)
	{
		if (BACKWARD) { boost = boostLevel; stop(); }        // only apply boost when direction changes
		forward();
		BACKWARD = false;
	}
	else
	{
		if (!BACKWARD) { boost = boostLevel; stop(); }       // only apply boost when direction changes
		backward();
		BACKWARD = true;
	}
}

void controlPower()
{
	double calcTickMotor = abs(tickMotor);
	double calcTickPower = (52.0 + calcTickMotor / tickPwm) * boost;
	
	pwm  = (int) calcTickPower;
	
	if (boost > 1.0) { boost -= boostStep; }     // Step down boost until it equals 1.0 
}

/* IMU routine ==================================================================
The DMP performs sensor fusion, quaternation, gravity and Euler calculations 
================================================================================== */

void getAngle()
{
	fifoCount = imu.getFIFOCount();
	if (fifoCount == 1024) { imu.resetFIFO(); }
	else
	{
		while (fifoCount < packetSize) { fifoCount = imu.getFIFOCount(); }
		imu.getFIFOBytes(fifoBuffer, packetSize);
		fifoCount -= packetSize;
		imu.dmpGetQuaternion(&q, fifoBuffer);
		imu.dmpGetGravity(&gravity, &q);
		imu.dmpGetYawPitchRoll(ypr, &q, &gravity);
	}

	angle = ypr[1];
	pitch = movingAverageAngle.process(angle);
	
	//Serial.println(pitch);
}

/* =================================================================================
Encoder routines
==================================================================================== */

void getEncoders(void)
{
	encValRight = encMotorRight.read() * -1; encValLeft = encMotorLeft.read();

	if (encValRight >= 30000 || encValRight <= -30000) { resetEncoders(); } // Prevent Roll Over 
	if (encValLeft  >= 30000 || encValLeft  <= -30000) { resetEncoders(); }
}

void resetEncoders(void)
{
	encMotorRight.write(0); encMotorLeft.write(0);
	encValRight = 0;        encValLeft = 0;
	encValRightLast = -1 * tickSpeedRight;  encValLeftLast = -1 * tickSpeedLeft;
}

/* =================================================================================
Motor routines
==================================================================================== */

void throttle()
{
	pwm  = constrain(pwm, 52, 255);
	analogWrite(SPEEDPIN_B, pwm);
	analogWrite(SPEEDPIN_A, pwm);
}

void forward()
{
	digitalWrite(MOTOR_PIN1, LOW); digitalWrite(MOTOR_PIN2, HIGH);  // right motor
	digitalWrite(MOTOR_PIN3, HIGH);  digitalWrite(MOTOR_PIN4, LOW); // left motor
}

void backward()
{
	digitalWrite(MOTOR_PIN1, HIGH); digitalWrite(MOTOR_PIN2, LOW);  // right motor
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, HIGH); // left motor
}

void stop()
{
	analogWrite(SPEEDPIN_A, 0); analogWrite(SPEEDPIN_B, 0);
	//digitalWrite(MOTOR_PIN1, LOW);  digitalWrite(MOTOR_PIN2, LOW);
	//digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, LOW);
}

void turning(int dir, int degrees)
{

}

/* ==================================================================================
Communication routines
================================================================================== */

void getMessage()
{
	int messageID = 0;
	int messageOp = 0;
	int value = 0;
	
	if (Serial.available() > 0)
	{
		int incomingSerialBytes = Serial.available();
		if (incomingSerialBytes == 5)
		{
			messageID = Serial.read()- 48;
			messageOp = Serial.read()- 48;
			for (int i = 0; i < 3; i++)
			{
				value = value * 10 + Serial.read() - 48;
			}
			MESSAGE = true;
		}
		Serial.flush();
	}
	
	if (Bluetooth.available() > 0)
	{
		char incomingByte = Bluetooth.read();

		switch (incomingByte)
		{
		case 'q': KpAngle += 0.05;   UPDATE = true; break;
		case 'w': KpAngle -= 0.05;   UPDATE = true; break;
		case 'a': KdAngle += 0.01;  UPDATE = true; break;
		case 's': KdAngle -= 0.01;  UPDATE = true; break;
		case 'z': boostLevel += 0.1; boostStep = (boostLevel - 1) / 2; BACKWARD = !BACKWARD; break;
		case 'x': boostLevel -= 0.1; boostStep = (boostLevel - 1) / 2; BACKWARD = !BACKWARD; break;
			
		//case 'o': KpSpeed += 0.01; UPDATE = true; break;
		//case 'p': KpSpeed -= 0.01; UPDATE = true; break;
		//case 'k': KiSpeed += 0.01; UPDATE = true; break;
		//case 'l': KiSpeed -= 0.01; UPDATE = true; break;
		//case 'n': KdSpeed += 0.01; UPDATE = true; break;
		//case 'm': KdSpeed -= 0.01; UPDATE = true; break;

		case '#': stop(); resetRobot(); break;
		//case '0': speedPerc  = 0;  setTickTarget(); resetEncoders(); break;
		//case '1': speedPerc += 10; setTickTarget(); break;
		//case '2': speedPerc -= 10; setTickTarget(); break;
		}
	}
	
	if (UPDATE)
	{
		PidAngle.SetTunings(KpAngle, KiAngle, KdAngle);
		PidSpeed.SetTunings(KpSpeed, KiSpeed, KdSpeed);
		UPDATE = false;
	}
	
	if (MESSAGE)
	{
		switch (messageID)
		{
			case 0: sendSensorValues(); break;
			case 1: speedPerc = value; setTickTarget(); break; 
			case 2: turning(messageOp, value); break;
			case 9: resetRobot; break;
		}
		MESSAGE = false;
	}
}

void sendSensorValues()
{
	// construct the sensor values
}


void ledLight()
{
	if (ledOFF) { digitalWrite(LEDPIN, LOW); }
	else { digitalWrite(LEDPIN, HIGH);  }
}

void blink()
{
	for (int x = 0; x < 15; x++)
	{
		ledOFF = false;
		ledLight();
		delay(100);
		ledOFF = true;
		ledLight();
		delay(100);
	}
}

void tab()  { Serial.print("\t"); }
void line() { Serial.print("\n"); }

/* ==================================================================================
Robot State routines
================================================================================== */

void resetRobot()
{
	stop();

	PidSpeed.SetMode(MANUAL);
	PidAngle.SetMode(MANUAL);

	delay(1000);
	
	ledOFF = true;
	ledLight();

	do
	{
		getAngle(); 
		getMessage();
		delay(100);
	} while (pitch <-deadAngle + 0.7 || pitch > deadAngle - 0.7);

	initRobot();
	delay(2000);
}

void initRobot()
{
	speedPerc = 0;
	setTickTarget();
	tickComp = 0; tickMotor = 0;
	tickSpeedRight = 0; encValRightLast = 0;
	tickSpeedLeft  = 0; encValLeftLast = 0;
	tickAngular = 0; pitch = 0;
	resetEncoders(); 
	for (int x = 0; x < 15; x++) { getAngle(); }           // To set pitch to current position
	PidSpeed.SetMode(AUTOMATIC);
	PidAngle.SetMode(AUTOMATIC);
	controlAngle();
	controlSpeed();
	boost = boostLevel;
	ledOFF = false;
	ledLight();
}
