/*==================================================================================
This script works as 'firmware' for a 2-wheeled balancer.

Bluetooth control is used for tuning and basic testing of turning & runing.
Also 2 potmeters are added for first (ballpark) tuning of the gains.
The final version will have full serial control by a Raspberry Pi. (ToDo)

Functions:
- IMU (MPU6050) handling for vertical & differential control;
- Horizontal control using wheel encoders;
- Vertical control using IMU pitch value;
- Differential control using IMU yaw values;
- Bluetooth communication for gain tuning, output monitoring and motion testing
- Trimpots for gain tuning
- USB serial communication for command & status updates from/to a Raspberry Pi (ToDo)

Hardware:
- Pololu Low Power DC motors (2282: 590 rpm, 9.68:1, 39 oz-in)
- Hall encoders 464.64 cpr (48 cpr after gear )
- Pololu wheels 120  x 60 mm
- Arduino UNO
- Seeed Studio 4A motor shield (Mind the pin settings; Seeed example library uses wrong wiring)
- Dagu bluetooth module (HC06)

The motor shield uses pin 9 & 10 as PWM pins, the encoders use pin 3 and 4 for
interrupts. So this configuration cannot also handle servo's.
(servo.h = timer1/D9+10, servoTimer2.h = timer2/D3+11)
Servo's (camera, radar, raising mechanism) will be controlled by the Rpi through
an Ultraborg board.

Pinsettings ========================================================================

D0 = ---- (Rx)         D6 = motor IN2         D12 = red led   A0 = bluetooth TX
D1 = ---- (Tx)         D7 = motor IN3         D13 = blue led  A1 = bluetooth RX
D2 = Right encoder A   D8 = motor IN4                         A2 =                     //trimpot 1 (D)
D3 = Left  encoder A   D9 = motor PWM A       SDA = n/a       A3 =                     //trimpot 2 (P)
D4 = Right encoder B   D10 = motor PWM B      SCL = n/a       A4 = IMU   (SDA)
D5 = motor IN1         D11 = Left encoder B                   A5 = IMU   (SCL)

Motors =============================================================================

pololunr        : 2274
type            : 46.85:1 HP
gearboxRatio    : 46.85
no load speed   : 200 rpm @ 6.0V (data sheet),   258.93 @ at 7.94V (Lipo)
ticksRevolution : 2248.80
torque          : 115 oz/in

wheel diameter  : 0.130 m
wheel perimeter : 0.408 m
wheel distance  : 0.238 m  (center wheels)

average friction in pwm-values:
                  no-load  loaded
forward - left      40       
forward - right     44       
backward - left     40       
backward - right    46       

-----------------------------------------------------------------
=> ticksPerMeter    = ticks/revolution * wheel perimeter
=> ticksPerDegree	= ticks per revolution / 360
=> maxTicksPerInt   = RPM/60 * ticks/revolution * 0.02
=> maxDegreesPerSec = RPM/60 * 360
=> degrees2pwm      = maxDegreesPerSec /255 * 0.02  (Adjusted to 50Hz)
=> yaw2pwm          = Pi * wheel_distance * ticksPerMeter/ 360 * maxTicksPerInt / 255   ;-0)

IMU ================================================================================

The Digital Motion Processor (DMP) of the MPU6050 is used:
- Because it's there
- Saves the effort of coding the calculations for angle and filter
- Avoids CPU/memory use of the Arduino
- Performs self calibration

Downside: Self calibration requires a substantial initiation time (> 10 seconds)

Note: The 'hack'of Jeff Rowberg only works with MPU6050 and MPU9150
Although I adjusted the register settings, I couldn't get DMP
working properly with a MPU9250 imu (So, no thanks to Invensense)

Messaging ==========================================================================

HardwareSerial for communication with the Raspberry Pi
SoftwareSerial for bluetooth communication with the android remote

Control loop cascade ===============================================================

Outer loop: PD control horizontal (0.02s)       Inner loop: PD control vertical (0.02s)
-----------------------------------------       ---------------------------------------

target = required displacement (in ticks)
input  = ticks made by encoders
output = compensating ticks (as an angle)  -->  target  = required pitch angle
                                                input   = angle from IMU
                                                output  = compensating angle

A separate (PD) control loop is used for differential control

====================================================================================
Program size: 25.412 bytes (used 79% of a 32.256 byte maximum) (1,46 secs)
Minimum Memory Usage: 855 bytes (42% of a 2048 byte maximum)
====================================================================================*/

#include <Encoder.h>
#include <math.h>
#include <SoftwareSerial.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include "I2Cdev.h"
#include <avr/wdt.h> 

// Pinsettings =====================================================================

const unsigned int MOTOR_PIN1 = 5;
const unsigned int MOTOR_PIN2 = 6;
const unsigned int MOTOR_PIN3 = 7;
const unsigned int MOTOR_PIN4 = 8;
const unsigned int SPEEDPIN_R = 9;
const unsigned int SPEEDPIN_L = 10;
const unsigned int LEDREDPIN  = 12;
const unsigned int LEDBLUEPIN = 13;

// Speed ===========================================================================

const double rad2degr       = 57.2957795;
                                             //Robot dependant constants
const double ticksPerMeter  = 5506.27132;
const double ticksPerDegree = 6.24666667;
const double yaw2pwm        = 0.30050281;

                                             //RPM and time dependant constants
											 //Interval = 0.02 seconds
const double maxTicksPerInt = 194.0901836;
const double degrees2pwm    = 0.121847061;

int turnTarget, speedTarget;
double speedOffset;

// Power ===========================================================================

int yawCompLeft, yawCompRight;

int slackFwLeft = 39; int slackFwRight = 44;          //Only a variable while tuning
int slackBwLeft = 39; int slackBwRight = 44;          //Only a variable while tuning 

double pwmLeft, pwmRight, pwmLastLeft, pwmLastRight; 

// Encoders =========================================================================

Encoder encMotorRight(3, 11);
Encoder encMotorLeft(2, 4);

int encValLeft, encValRight;
double ticksMade, ticksMadeOld, wheelVelocity;

// IMU ==============================================================================

MPU6050 imu;
I2Cdev   I2C_M;

bool dmpReady = false;  // set true if DMP init was successful

uint8_t devStatus;      // return status after each DMP operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

const double deadAngle  = 20.0;
const double pitchOffset = 0.8;							  //Only a variable while tuning

double pitchCurrent, pitchOld, yawCurrent, yawOld, angleVelocity;

// Messaging ========================================================================

SoftwareSerial Bluetooth(A1, A0); // RX, TX

char messageIdBluetooth = ' ';
String messageInBluetooth = "";

const int BLUE = 1;
const int RED = 2;

// Timing ===========================================================================

double interval;
long timerLast;

// Control loops ====================================================================

double KpHor = 0.0; double KpPitch = 0.55 ; double KpYaw = 0.0;
double KdHor = 0.0; double KdPitch = 0.0  ; double KdYaw = 0.0;

double tickTarget, tickError;
double pitchTarget, pitchError, compAngle;
double yawTarget, yawError;

// State ============================================================================

boolean MESSAGE = false;
boolean FORWARD = false;

//===================================================================================

void setup()
{
	Wire.begin();
	Serial.begin(115200); while (!Serial) {}
	Bluetooth.begin(9600);

	pinMode(MOTOR_PIN1, OUTPUT);
	pinMode(MOTOR_PIN2, OUTPUT);
	pinMode(MOTOR_PIN3, OUTPUT);
	pinMode(MOTOR_PIN4, OUTPUT);
	pinMode(SPEEDPIN_R, OUTPUT);
	pinMode(SPEEDPIN_L, OUTPUT);
	pinMode(LEDBLUEPIN, OUTPUT);
	pinMode(LEDREDPIN,  OUTPUT);

	imu.initialize(); delay(1000);

	int connection = imu.testConnection();
	if (connection == 1) { blink(BLUE); }
	else { blink(RED); wdt_enable(WDTO_500MS); while (1) {} }

	devStatus = imu.dmpInitialize();
		
	imu.setXAccelOffset(-1765);                  // chip default: = -1684
	imu.setYAccelOffset(-2777);                  // chip default: = -2787
	imu.setZAccelOffset(1165);                   // chip default: =  1044
	imu.setXGyroOffset(96);                      // chip default: =     0
	imu.setYGyroOffset(-32);                     // chip default: =     0
	imu.setZGyroOffset(24);                      // chip default: =     0

	if (devStatus == 0)
	{
		imu.setDMPEnabled(true);
		dmpReady = true;
		packetSize = imu.dmpGetFIFOPacketSize();  // get expected DMP packet size for later comparison
		blink(BLUE);
	}
	else { blink(RED); wdt_enable(WDTO_500MS); while (1) {} }

	delay(15000);                     // Setling time for the DMP
	blink(BLUE);

	//wdt_enable(WDTO_2S);              // Set watchdog timer 

	resetRobot();
}

//===================================================================================

void loop()
{
	if (pitchCurrent <-deadAngle || pitchCurrent > deadAngle) { resetRobot(); }

	getMessage();                   // Check for user (bluetooth) or Raspberry commands 
	getTicksMade();                 // Read encoders
	getAngles();                    // Read IMU, get pitch and yaw angle

	long timerNow = millis();
	interval = ((double) timerNow - (double) timerLast) / 1000;
	
	if (interval >= 0.01)           // Enforce control loop timing
	{
		timerLast = timerNow;		
						
		horizontalControl();        // Compensate for target displacement
		verticalControl();          // Compensate for target vertical angle
		setDirection();             // Set motors to forward or backward
		differentialControl();      // Compensate for target differential angle
		setPower();                 // Compute PWM value
		throttle();                 // Rotate motors
	}
}

/* =================================================================================
Control routines
==================================================================================== */

inline void horizontalControl()
{
	tickTarget += speedOffset;

	tickError = tickTarget - ticksMade;
	wheelVelocity = (ticksMade - ticksMadeOld) / interval;
	double compTicks = KpHor * tickError - KdHor * wheelVelocity;  //Derivative on Measurement
	ticksMadeOld = ticksMade;

	pitchTarget = compTicks / ticksPerDegree * interval;
}

inline void verticalControl()
{
	pitchError = pitchTarget - pitchCurrent;
	angleVelocity = (pitchCurrent - pitchOld) / interval;
	
	compAngle = KpPitch * pitchError - KdPitch * angleVelocity;     //Derivative on Measurement

	pitchOld = pitchCurrent;
}

inline void differentialControl()
{
	//Set error to a clockwise value in the range 0/360
	if (yawCurrent < 180) { yawError = 0 - yawCurrent; }
	else { yawError = 360.0 - yawCurrent; }
	
	//Adjust error by the target offset
	if (yawError <= 180) { yawError += yawTarget; }
	else { yawError -= yawTarget; }

	//Select the smallest error/angle-to-turn
	if (yawError > 180) { yawError -= 360.0; }
	else if (yawError < -180) { yawError += 360.0; }

	//Control loop
	double yawComp = (KpYaw * yawError) - (KdYaw * (yawCurrent - yawOld) / interval);
	yawComp *= yaw2pwm;
	yawOld = yawCurrent;

	//Round-up to an integer  
	yawCompLeft = (yawComp + 0.5); yawCompRight = (yawComp + 0.5);

	//Adjust for direction
	if (FORWARD) { yawCompRight *= -1; }
	else { yawCompLeft *= -1; }
}

inline void setDirection()
{
	if (pitchCurrent > 0) { forward(); }
	else if (pitchCurrent < 0) { backward(); }
	else { neutral(); }
}

inline void setPower()
{
	//Convert to an absolute value
	if (compAngle < 0) { compAngle *= -1; }

	//Match PID output for pitch and velocity
	pwmLeft = compAngle / degrees2pwm; 
	pwmRight = compAngle / degrees2pwm;

	//Adjust for motor stiction
	if (FORWARD) { pwmLeft += slackFwLeft; pwmRight += slackFwRight; }
	else         { pwmLeft += slackBwLeft; pwmRight += slackBwRight; }

	//Adjust by differential drift and/or angle to turn 
	pwmLeft += yawCompLeft; pwmRight += yawCompRight;
}

inline void setSpeedTarget()
{
	double speedPerc = constrain((double)speedTarget, -100.0, 100.0) * 0.01;
	speedOffset = speedPerc * maxTicksPerInt;
}

inline void setTurnTarget()
{
	//Adjust the target offset by the angle to turn
	yawTarget += (double)turnTarget;
	if (yawTarget > 360) { yawTarget -= 360.0; }
	else if (yawTarget < 0) { yawTarget += 360.0; }
	turnTarget = 0;
}

/* ===============================================================================
Sensor routines
================================================================================== */

inline void getAngles()
{
	fifoCount = imu.getFIFOCount();
	while (fifoCount < packetSize) { fifoCount = imu.getFIFOCount(); }

	imu.getFIFOBytes(fifoBuffer, packetSize);
	fifoCount -= packetSize;
	imu.dmpGetQuaternion(&q, fifoBuffer);
	imu.dmpGetGravity(&gravity, &q);
	imu.dmpGetYawPitchRoll(ypr, &q, &gravity);

	pitchCurrent = ypr[1] * rad2degr + pitchOffset;
	yawCurrent   = ypr[0] * rad2degr;

	if (yawCurrent < 0) { yawCurrent += 360; }        //adjust to (forward) clockwise range 0-360 
		
	imu.resetFIFO();
}

inline void getTicksMade(void)
{
	encValRight = encMotorRight.read() * -1; encValLeft = encMotorLeft.read();

	if (encValRight >= 30000 || encValRight <= -30000) { resetEncoders(); } // Prevent Roll Over 
	if (encValLeft >= 30000 || encValLeft <= -30000) { resetEncoders(); }

	ticksMade = ((double)encValLeft + (double)encValRight) / 2.0;
}

inline void resetEncoders(void)
{
	encMotorRight.write(0); encMotorLeft.write(0);
	encValRight = 0;        encValLeft = 0;

	ticksMadeOld = -1 * wheelVelocity;
}

/* =================================================================================
Motor routines
==================================================================================== */

inline void throttle()
{
	int pwmL = int(pwmLeft + 0.5);
	pwmL = constrain(pwmL, 0, 255);
	int pwmR = int(pwmRight + 0.5);
	pwmR = constrain(pwmR, 0, 255);
	analogWrite(SPEEDPIN_L, pwmL);
	analogWrite(SPEEDPIN_R, pwmR);
}

inline void forward()
{
	FORWARD = true;
	digitalWrite(MOTOR_PIN1, HIGH); digitalWrite(MOTOR_PIN2, LOW);  // right motor
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, HIGH); // left motor
}

inline void backward()
{
	FORWARD = false;
	digitalWrite(MOTOR_PIN1, LOW); digitalWrite(MOTOR_PIN2, HIGH);  // right motor
	digitalWrite(MOTOR_PIN3, HIGH);  digitalWrite(MOTOR_PIN4, LOW); // left motor
}

void neutral()
{
	digitalWrite(MOTOR_PIN1, LOW);  digitalWrite(MOTOR_PIN2, LOW);
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, LOW);
}

void stop()
{
	analogWrite(SPEEDPIN_R, 0); analogWrite(SPEEDPIN_L, 0);
	neutral();
}

/* ==================================================================================
Interaction routines
================================================================================== */

inline void getMessage()
{
	//wdt_reset();                          //Prevent watchdog to reset the Arduino

	int messageIDSerial = 0;
	int messageOpSerial = 0;
	int value = 0;

	if (Serial.available() > 0)
	{
		int incomingSerialBytes = Serial.available();
		if (incomingSerialBytes == 5)
		{
			messageIDSerial = Serial.read() - 48;
			messageOpSerial = Serial.read() - 48;
			for (int i = 0; i < 3; i++)
			{
				value = value * 10 + Serial.read() - 48;
			}
			//MESSAGE = true;
		}
		Serial.flush();
	}

	if (Bluetooth.available() > 0)
	{
		messageIdBluetooth = Bluetooth.read();
		messageInBluetooth = "";
		char incomingChar = Bluetooth.read();
		while (incomingChar != '\n')
		{
			messageInBluetooth = messageInBluetooth + incomingChar;
			incomingChar = Bluetooth.read();
		}
		processBluetoothMessage();
	}

	if (MESSAGE)
	{
		switch (messageIDSerial)
		{
		case 0: sendSensorValuesToRpi(); break;
		case 1: speedTarget = value; break;
		case 2: setTurnTarget(); break;
		case 9: resetRobot; break;
		}
		MESSAGE = false;
	}
}

inline void processBluetoothMessage()
{

	switch (messageIdBluetooth)
	{
	case 'H': speedTarget = messageInBluetooth.toInt(); setSpeedTarget(); break;
	case 'J': turnTarget = messageInBluetooth.toInt();   setTurnTarget(); break;

	//Following variables only used for tuning. Could be constants

	case 'A': KpPitch = messageInBluetooth.toFloat();     break;
	case 'C': KdPitch = messageInBluetooth.toFloat();     break;
	case 'D': KpHor = messageInBluetooth.toFloat();       break;
	case 'E': KdHor = messageInBluetooth.toFloat();       break;
	case 'F': KpYaw = messageInBluetooth.toFloat();       break;
	case 'G': KdYaw = messageInBluetooth.toFloat();       break;

	//Following variables only used for tuning. Changed into constants

	case 'K': slackFwLeft = messageInBluetooth.toInt();   break; 
	case 'L': slackFwRight = messageInBluetooth.toInt();  break;
	case 'M': slackBwLeft = messageInBluetooth.toInt();   break;
	case 'N': slackBwRight = messageInBluetooth.toInt();  break;
	//case 'O': pitchOffset = messageInBluetooth.toFloat(); break; 

	//Following values are used for updating the Android screen

	case 'a': Bluetooth.println(KpPitch, 3);     break;
	case 'c': Bluetooth.println(KdPitch, 3);     break;
	case 'd': Bluetooth.println(KpHor, 3);       break;
	case 'e': Bluetooth.println(KdHor, 3);       break;
	case 'f': Bluetooth.println(KpYaw, 3);       break;
	case 'g': Bluetooth.println(KdYaw, 3);       break;
	case 'j': Bluetooth.println(yawTarget, 3);   break;
	case 'k': Bluetooth.println(slackFwLeft);    break;
	case 'l': Bluetooth.println(slackFwRight);   break;
	case 'm': Bluetooth.println(slackBwLeft);    break;
	case 'n': Bluetooth.println(slackBwRight);   break;
	case 'o': Bluetooth.println(pitchOffset, 2); break;
	//case 'x': Bluetooth.println(pitchError, 3);  break;
	case 'x': Bluetooth.println(pitchCurrent, 3);  break;
	}
}

void sendSensorValuesToRpi()
{
	// construct the sensor values; probably only room left for 1 distance sensor
}

void ledOn(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, HIGH); }
	else { digitalWrite(LEDREDPIN, HIGH); }
}

void ledOff(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, LOW); }
	else { digitalWrite(LEDREDPIN, LOW); }
}

void blink(int color)
{
	for (int x = 0; x < 5; x++) { ledOn(color); delay(100); ledOff(color); delay(100); }
}

/* ==================================================================================
Robot State routines
================================================================================== */

void resetRobot()
{
	stop(); neutral();

	ledOff(BLUE); ledOff(RED);

	// Initialize speed variables
	speedTarget = 0;
	turnTarget = 0;
	speedOffset = 0.0;

	// Initialize control loop variables
	wheelVelocity = 0.0; pitchTarget = 0.0; ticksMadeOld = 0.0;
	angleVelocity = 0.0; compAngle = 0.0; pitchOld = 0.0;
	yawCompLeft = 0; yawCompRight = 0; yawOld = 0.0;

	// Initialize power variables
	pwmLeft = 0.0; pwmRight = 0.0;
	pwmLastLeft = 0.0; pwmLastRight = 0.0;
	yawCompLeft = 0;   yawCompRight = 0;

	// Initialize encoder variables
	resetEncoders();

	// Wait untill standing (almost) straight
	do
	{
		getAngles();
		getMessage();
	} while (pitchCurrent <-deadAngle || pitchCurrent > deadAngle);

	ledOn(BLUE);

	getTicksMade();                                     //Works better than a mere reset.
	tickTarget = ticksMade;
	ticksMadeOld = ticksMade;

	getAngles();
	yawTarget = yawCurrent;                             //Take yawTarget as offset to 0/360
	yawOld = yawCurrent;

	pitchOld = pitchCurrent;

	timerLast = millis(); interval = 0.0;              //Initialize control loop timer
}
