/*==================================================================================
This script operates as 'firmware' for a RPi controlled balancer. The complete 
sketch also handles serial communication over usb with the Rpi

Functions:
- IMU handling (gyroscope & accelerometer. Magnetometer heading will be  
  added in a future version )
- Encoder handling
- Motor handling (balancing, forward, backward and turning)
- Bluetooth communication for gain tuning and motion alternative control
- USB serial communication for command & status updates 

Hardware used:
- Pololu High Power DC motors 210 rpm, 47:1, 80 oz-in
- Hall encoders 2248.8 cpr (48 cpr after gear )
- Pololu wheels 90  x 10 mm
- Arduino UNO
- Seeed Studio 4A motor shield
- Dagu bluetooth module

====================================================================================

Program size: 25.678 bytes (used 80% of a 32.256 byte maximum) (2,89 secs)
Minimum Memory Usage: 996 bytes (49% of a 2048 byte maximum)

====================================================================================*/

#include <math.h>
#include <TimedAction.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include "I2Cdev.h"

// Pinsettings =====================================================================
// Motors ===========================================================================
// Encoders =========================================================================
// IMU ==============================================================================

const double rad2degr = 57.2957795;

MPU6050 imu;
I2Cdev   I2C_M;

bool dmpReady = false;  // set true if DMP init was successful

uint8_t devStatus;      // return status after each DMP operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

double pitchCurrent, pitchPrevious;

// Messaging ========================================================================

// Timing ===========================================================================

TimedAction VerticalControl = TimedAction(10, verticalControl);
unsigned long lastTime = 0;

// Control loops ====================================================================
// Filters ==========================================================================
// State ============================================================================
//===================================================================================

void setup()
{
	Wire.begin();
	Serial.begin(115200); while (!Serial) {}

	imu.initialize();
	delay(1000);
	Serial.println(imu.testConnection() ? "Connected" : "FAILED !!");
	int connection = imu.testConnection();
	if (connection == 1) { Serial.println("Connected"); }
	else { Serial.println("FAILED !!"); }

	devStatus = imu.dmpInitialize();

	imu.setXAccelOffset(-1684);     // Chip default = -1684
	imu.setYAccelOffset(-2877);     // Chip default = -2787
	imu.setZAccelOffset(1044);      // Chip default = 1044
	imu.setXGyroOffset(0);         // Chip default = 0
	imu.setYGyroOffset(0);        // Chip default = 0
	imu.setZGyroOffset(0);         // Chip default = 0

	if (devStatus == 0)
	{
		imu.setDMPEnabled(true);
		dmpReady = true;
		packetSize = imu.dmpGetFIFOPacketSize();  // get expected DMP packet size for later comparison
	}
	else
	{
		Serial.println("No DMP   ");
		Serial.print(F("DMP Initialization failed (code "));
		Serial.print(devStatus);
		Serial.println(F(")"));
	}

	Serial.println("Calibrate");
	delay(15000);                                   // Setling time for the DMP
	Serial.println("IMU ready");
	lastTime = millis();
}


//===================================================================================

void loop()
{
	getAngle();
	VerticalControl.check();
}

/* =================================================================================
Control routines
==================================================================================== */

void verticalControl()
{
	pitchPrevious = pitchCurrent;
}

/* ===============================================================================
Sensor routines
================================================================================== */

void getAngle()
{
	unsigned long currentTime = millis();
	double seconds = ((double) currentTime - (double) lastTime) / 1000;
	lastTime = currentTime;
	
	fifoCount = imu.getFIFOCount();
	if (fifoCount == 1024) { imu.resetFIFO(); }
	else
	{
		while (fifoCount < packetSize) { fifoCount = imu.getFIFOCount(); }
		imu.getFIFOBytes(fifoBuffer, packetSize);
		fifoCount -= packetSize;
		imu.dmpGetQuaternion(&q, fifoBuffer);
		imu.dmpGetGravity(&gravity, &q);
		imu.dmpGetYawPitchRoll(ypr, &q, &gravity);
	}

	pitchCurrent = ypr[1];
	pitchCurrent *= rad2degr;
	double yawCurrent = ypr[0];
	yawCurrent *= rad2degr;
	double rollCurrent = ypr[2];
	rollCurrent *= rad2degr;
	Serial.print(seconds,2); tab();
	Serial.print(pitchCurrent,3); tab(); 
	Serial.print(rollCurrent, 3); tab();
	Serial.println(yawCurrent, 2);
	delay(50);
}

/* ===============================================================================
Encoder routines
================================================================================== */
/* =================================================================================
Motor routines
==================================================================================== */
/* ==================================================================================
Communication routines
================================================================================== */

void tab()  { Serial.print("\t"); }
void com() { Serial.print(","); }

/* ==================================================================================
Robot State routines
================================================================================== */

