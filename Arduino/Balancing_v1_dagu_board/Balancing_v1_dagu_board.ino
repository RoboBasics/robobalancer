/*
Sketch size: 14,536 bytes (45% of 32,256 bytes)
Global variables use 1067 bytes (52% of 2,048 bytes dynamic memory) 
*/

#include "Wire.h"
#include "I2Cdev.h"
#include "MPU6050.h"
#include <PololuWheelEncoders.h>
#include <math.h>
#include <Servo.h>
#include <PID_v1.h>
#include <TimedAction.h>

const int LEFT_MOTOR_DIR_PIN = 10;
const int LEFT_MOTOR_PWM_PIN = 5;
const int RIGHT_MOTOR_DIR_PIN = 11;
const int RIGHT_MOTOR_PWM_PIN = 6;
const int BATTERY_VOLTAGE_PIN = A7;
const int SWEEP_SERVO_PIN = 12 ;
const int PAN_SERVO_PIN = 8 ;
const int TILT_SERVO_PIN = 9 ;
const int LED_PIN = 13;

const int CENTRE_PAN = 80;
const int CENTRE_TILT = 100;
const int CENTRE_SWEEP = 90;

const int trimpotPin1 = A1;
const int trimpotPin2 = A2;
const int trimpotPin3 = A3;

const int deadAngle = 20;

const double maxMeterSec = 0.50; // Theoretical 0.63
const double tickMeter = 363.78;
const double meterSecDegreeRatio = 0.17;
const double pwmMeterSecRatio = 463.64;

int tickRightLast = 0;
int tickLeftLast = 0;

int inputSpeedPerc = 0;
int speedLeft = 0;
int speedRight = 0;
int powerLeft = 0;
int powerRight = 0;

MPU6050 accelgyro;
PololuWheelEncoders enc;
I2Cdev   I2C_M;

unsigned char m1a = 4;
unsigned char m1b = 2;
unsigned char m2a = 3;
unsigned char m2b = 7;

int16_t ax, ay, az;
int16_t gx, gy, gz;
//int16_t mx, my, mz;                    //Magnetometer not used for balancing

double accelRoll, accelPitch, accelYaw;
double gyroPitch;
double angleTiltAccel, angleTiltFiltered, angleTiltFilteredSum; 
int countAngle = 0;

double angleTiltFilteredAvg, KpAngle, KiAngle, KdAngle;
double angleCompLeft, angleTargetLeft, angleCompRight, angleTargetRight;
double metersSecLeft, metersCompLeft, metersTarget, KpPosLeft, KiPosLeft, KdPosLeft;
double metersSecRight, metersCompRight, KpPosRight, KiPosRight, KdPosRight;

PID PIDAngleLeft(&angleTiltFilteredAvg, &angleCompLeft, &angleTargetLeft, 0, 0, 0, REVERSE);
PID PIDAngleRight(&angleTiltFilteredAvg, &angleCompRight, &angleTargetRight, 0, 0, 0, REVERSE);
PID PIDPosLeft(&metersSecLeft, &metersCompLeft, &metersTarget, 0, 0, 0, DIRECT);
PID PIDPosRight(&metersSecRight, &metersCompRight, &metersTarget, 0, 0, 0, DIRECT);

double meterSecOutputLeft = 0.0;
double meterSecOutputRight = 0.0;

TimedAction timedControlAngle = TimedAction(10, controlAngle);
TimedAction timedControlPos = TimedAction(20, controlPos);
//TimedAction timedSweepServo = TimedAction(40, moveSweepServo);

//int sweepPos = 90;
//int sweepIncrement = +1;

//Servo sweepServo;
//Servo panServo;
//Servo tiltServo;

enum sweepServoStatus { sweepON, sweepOFF, turnSweepON, turnSweepOFF };
sweepServoStatus sweepState = sweepOFF;

enum robotStatus {FORWARD, BACKWARD, BALANCING, STOP};
robotStatus robotState = BALANCING; 

unsigned long startTimeIMU;

void setup() {
  Wire.begin();
  Serial.begin(115200);
  
  accelgyro.initialize();
  delay(1000);
  Serial.println(accelgyro.testConnection() ? "Connected" : "FAILED !!");
  
  pinMode( LEFT_MOTOR_DIR_PIN, OUTPUT );
  pinMode( LEFT_MOTOR_PWM_PIN, OUTPUT );
  pinMode( RIGHT_MOTOR_DIR_PIN, OUTPUT );
  pinMode( RIGHT_MOTOR_PWM_PIN, OUTPUT );
  pinMode( LED_PIN, OUTPUT);
  
  enc.init(m1a,m1b,m2a, m2b);

  angleTargetLeft = 0.0;
  PIDAngleLeft.SetSampleTime(10);
  PIDAngleLeft.SetMode(AUTOMATIC);
  PIDAngleLeft.SetOutputLimits(-21, 21);

  angleTargetRight = 0.0;
  PIDAngleRight.SetSampleTime(10);
  PIDAngleRight.SetMode(AUTOMATIC);
  PIDAngleRight.SetOutputLimits(-21, 21);

  metersTarget= 0.0;  
  PIDPosLeft.SetSampleTime(20);
  PIDPosLeft.SetMode(AUTOMATIC);
  PIDPosLeft.SetOutputLimits(-0.50, 0.50);

  PIDPosRight.SetSampleTime(20);
  PIDPosRight.SetMode(AUTOMATIC);
  PIDPosRight.SetOutputLimits(-0.50, 0.50);

  //panServo.attach(PAN_SERVO_PIN);
  //tiltServo.attach(TILT_SERVO_PIN);
  //sweepServo.attach(SWEEP_SERVO_PIN);
  
  resetRobot();
  
  startTimeIMU = millis();
}

void loop() {
  if (angleTiltFilteredAvg <- deadAngle || angleTiltFilteredAvg > deadAngle)
    {
		angleCompLeft = 0.00; angleCompRight = 0.00; robotState = STOP;
	} 
  if (robotState == STOP) {resetRobot();}
  //if (sweepState == sweepON) {timedSweepServo.check(); }
  updateTiltAngle();
  timedControlAngle.check();
  timedControlPos.check();
  double batteryReading = analogRead( BATTERY_VOLTAGE_PIN );
  //Serial.print(batteryReading / 100); Serial.print(",");
}

void updateTiltAngle(void) {
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz); //change to getMotion9 when magnetometer is also in use
  
  double currentTime = millis();                                
  double elapsedTime  = (currentTime - startTimeIMU) * 0.001;
  startTimeIMU   = currentTime; 
  
  accelPitch  = (double) ax / 16384;
  accelRoll   = (double) ay / 16384; accelRoll += 0.010;
  accelYaw    = (double) az / 16384; accelYaw += 0.01;
  gyroPitch   = (double) (gy - 62.05) * 250 / 32768 + 0.0445;
  //-----------------------------------------------------------------------------------------
  // angleTiltAccel = (atan(accelPitch/sqrt(pow(accelRoll,2) + pow(accelYaw,2))))* (180.0/PI);
  // Note: Using the full formula at onces gave unpredictable results 
  //-----------------------------------------------------------------------------------------
  double roll2 = accelRoll * accelRoll; double yaw2 = accelYaw * accelYaw;
  double result = sqrt(roll2 + yaw2); result = accelPitch / result;
  angleTiltAccel = atan(result);
  angleTiltAccel *= -100;
  angleTiltAccel -= 0.045;

  angleTiltFiltered = 0.98 * (angleTiltFiltered + gyroPitch * elapsedTime) + 0.02 * angleTiltAccel;
  
  countAngle += 1;
  angleTiltFilteredSum = angleTiltFilteredSum + angleTiltFiltered;
  angleTiltFilteredAvg = angleTiltFilteredSum / countAngle; 
}

void controlAngle(void) {

	updateGains();

	PIDAngleLeft.Compute();
	PIDAngleRight.Compute();

	angleTiltFilteredSum = 0.00;
	angleTiltFilteredAvg = 0.00;
	countAngle = 0;

	meterSecOutputLeft = angleCompLeft * meterSecDegreeRatio * 100;
	meterSecOutputRight = angleCompRight * meterSecDegreeRatio * 100;
	speedLeft = meterSecOutputLeft;
	speedRight = meterSecOutputRight;

	if (angleCompRight >= 0.00) { digitalWrite(RIGHT_MOTOR_DIR_PIN, HIGH); }
	else { digitalWrite(RIGHT_MOTOR_DIR_PIN, LOW); }
	if (angleCompLeft >= 0.00) { digitalWrite(LEFT_MOTOR_DIR_PIN, HIGH); }
	else { digitalWrite(LEFT_MOTOR_DIR_PIN, LOW); }

	powerLeft = map(speedLeft, 0, 50, 9, 255); // 53, 35/255
	powerRight = map(speedRight, 0, 50, 19, 255); // 53, 35/255

	analogWrite(RIGHT_MOTOR_PWM_PIN, powerRight);
	analogWrite(LEFT_MOTOR_PWM_PIN, powerLeft);
}

void controlPos(void) {
  
  getEncoderValues();

  inputSpeedPerc = constrain(inputSpeedPerc, 0, 100);
  metersTarget += (double)inputSpeedPerc /100 * maxMeterSec * 0.02; 

  PIDPosLeft.Compute();
  PIDPosRight.Compute();

  double compensatingSpeed = metersCompLeft / 0.02;
  angleTargetLeft = compensatingSpeed / meterSecDegreeRatio * -1;
  compensatingSpeed = metersCompRight / 0.02;
  angleTargetRight = compensatingSpeed / meterSecDegreeRatio * -1;
}

void getEncoderValues(void) {

  int r_enc = enc.getCountsM1() * -1; 
  int l_enc = enc.getCountsM2();
  
  metersSecLeft = ((double) l_enc - (double) tickLeftLast) / tickMeter / 0.02 ;
  metersSecRight = ((double) r_enc - (double) tickRightLast) / tickMeter / 0.02;

  tickLeftLast = l_enc;
  tickRightLast = r_enc;
}

void updateGains(void) {
  KpAngle = analogRead(trimpotPin1)/ 512.000;
  KiAngle = analogRead(trimpotPin2)/ 512.000 * 0.1;
  KdAngle = analogRead(trimpotPin3)/ 512.000 * 0.1;
  PIDAngleRight.SetTunings(KpAngle, KiAngle, KdAngle);
  PIDAngleRight.SetTunings(KpAngle, KiAngle, KdAngle);
  
  KpPosLeft = 0.0;
  KiPosLeft = 0.0;
  KdPosLeft = 0.0;
  PIDPosLeft.SetTunings(KpPosLeft, KiPosLeft, KdPosLeft);

  KpPosRight = 0.0;
  KiPosRight = 0.0;
  KdPosRight = 0.0;
  PIDPosRight.SetTunings(KpPosRight, KiPosRight, KdPosRight);
}

void resetRobot(void) {
  //panServo.write(CENTRE_PAN);
  //tiltServo.write(CENTRE_TILT); 
  //sweepServo.write(CENTRE_SWEEP);
    
  analogWrite( LEFT_MOTOR_PWM_PIN, 0 );
  analogWrite( RIGHT_MOTOR_PWM_PIN, 0 );

  PIDAngleLeft.SetMode(MANUAL);
  PIDAngleRight.SetMode(MANUAL);
  PIDPosLeft.SetMode(MANUAL); 
  PIDPosRight.SetMode(MANUAL);
  //sweepPos = 90;
  
  int rightEncoderValue = enc.getCountsAndResetM1();
  int leftEncoderValue = enc.getCountsAndResetM2();
  
  delay(1000);
  
  do {
       updateTiltAngle();
       updateGains();
       delay(50);
     } while (angleTiltFiltered <-deadAngle || angleTiltFiltered > deadAngle);
  
  angleTiltAccel = 0.00; angleTiltFiltered = 0.00; angleTiltFilteredSum = 0.00; angleTiltFilteredAvg = 0.00;
  countAngle = 0;
  
  robotState = BALANCING;
  
  for (int x=0; x<5; x++){
                            digitalWrite(LED_PIN, HIGH);
                            delay (500);
                            digitalWrite(LED_PIN, LOW);
                            delay(50);
                          }
  delay(100);

  PIDAngleLeft.SetMode(AUTOMATIC);
  PIDAngleRight.SetMode(AUTOMATIC);
  PIDPosLeft.SetMode(AUTOMATIC);
  PIDPosRight.SetMode(AUTOMATIC);
  
  startTimeIMU = millis();
}

void moveSweepServo() {
  //if (sweepPos >= 150) { sweepIncrement = -1;}
  //if (sweepPos <= 30) { sweepIncrement = +1;}
  //sweepPos += sweepIncrement;
  //sweepServo.write(sweepPos);
}

void switchSweepState (void) {
  if (sweepState == turnSweepON) {
      //sweepServo.attach(SWEEP_SERVO_PIN); 
      sweepState = sweepON; }
  else { 
    //sweepServo.detach();
    sweepState = sweepOFF; }
}
