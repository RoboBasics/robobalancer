/*==================================================================================
This script tests bluetooth and led wiring.
- Arduino UNO
- Dagu bluetooth module (HC06)

Pinsettings ========================================================================

A0 = bluetooth TX
A1 = bluetooth RX
A2 = blue led
A3 = red led

Messaging ==========================================================================

SoftwareSerial for bluetooth communication with the android remote

====================================================================================
Program size: 24.908 bytes (used 77% of a 32.256 byte maximum) (1,31 secs)
Minimum Memory Usage: 839 bytes (41% of a 2048 byte maximum)
====================================================================================*/

#include <SoftwareSerial.h>
#include "Wire.h"

// Pinsettings =====================================================================

const unsigned int LEDBLUEPIN = A2;
const unsigned int LEDREDPIN = A3;

// Messaging ========================================================================

SoftwareSerial Bluetooth(A1, A0); // RX, TX

char messageIdBluetooth = ' ';
String messageInBluetooth = "";

double testValue;

const int BLUE = 1;
const int RED = 2;

// State ============================================================================

boolean MESSAGE = false;

//===================================================================================

void setup()
{
	Wire.begin();
	Serial.begin(115200); while (!Serial) {}
	Bluetooth.begin(9600);

	pinMode(LEDBLUEPIN, OUTPUT);
	pinMode(LEDREDPIN, OUTPUT);

	testValue = 0.0;
}

//===================================================================================

void loop()
{
	blink(BLUE);
	blink(RED);

	getMessage();                   // Check for user (bluetooth) or Raspberry commands 
}

/* ==================================================================================
Interaction routines
================================================================================== */

void getMessage()
{
	if (Bluetooth.available() > 0)
	{
		messageIdBluetooth = Bluetooth.read();
		messageInBluetooth = "";
		char incomingChar = Bluetooth.read();
		while (incomingChar != '\n')
		{
			messageInBluetooth = messageInBluetooth + incomingChar;
			incomingChar = Bluetooth.read();
		}
		processBluetoothMessage();
	}
}

void processBluetoothMessage()
{
	switch (messageIdBluetooth)
	{
	case 'A': 
		testValue = messageInBluetooth.toFloat();
		blink(RED);
		Serial.println(testValue);
		break;
	
	case 'a': 
		Bluetooth.println(testValue, 3);     
		break;
	}
}

void ledOn(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, HIGH); }
	else { digitalWrite(LEDREDPIN, HIGH); }
}

void ledOff(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, LOW); }
	else { digitalWrite(LEDREDPIN, LOW); }
}

void blink(int color)
{
	for (int x = 0; x < 5; x++) { ledOn(color); delay(100); ledOff(color); delay(100); }
}
