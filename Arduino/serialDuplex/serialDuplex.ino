#include <SoftwareSerial.h>

// Messaging ========================================================================

SoftwareSerial Bluetooth(A1, A0); // RX, TX

char messageId = ' ';
String messageIn = "";

// Test variables====================================================================

int Stiction = 27;

double Pvert = 0.45000; double Dvert = 0.00300;
double Phor  = 1.32400; double Ihor  = 0.00123;
double Pdiff = 5.00005; double Idiff = 0.00009; double Ddiff = 0.00005;

void setup() 
{
	Serial.begin(115200); while (!Serial) {}
	Bluetooth.begin(9600);	
}

void loop()
{
	getMessage();
}

/* ==================================================================================
Communication routines
================================================================================== */

void getMessage()
{
	if (Bluetooth.available() > 0)
	{
		int incomingSerialBytes = Bluetooth.available();
		messageId = Bluetooth.read();
		messageIn = "";
		char incomingChar = Bluetooth.read();
		while (incomingChar != '\n')
		{
			messageIn = messageIn + incomingChar;
			incomingChar = Bluetooth.read();
		}
		processBluetoothMessage();
    }
}

void processBluetoothMessage()
{
	
	switch (messageId)
	{
	case 'A': Pvert = messageIn.toFloat();  break;
	case 'a': Bluetooth.println(Pvert, 5);  break;
	case 'B': Dvert = messageIn.toFloat();  break;
	case 'b': Bluetooth.println(Dvert, 5);  break;
	case 'C': Phor = messageIn.toFloat();   break;
	case 'c': Bluetooth.println(Phor, 5);   break;
	case 'D': Ihor = messageIn.toFloat();   break;
	case 'd': Bluetooth.println(Ihor, 5);   break;
	case 'E': Pdiff = messageIn.toFloat();  break;
	case 'e': Bluetooth.println(Pdiff, 5);  break;
	case 'F': Idiff = messageIn.toFloat();  break;
	case 'f': Bluetooth.println(Idiff, 5);  break;
	case 'G': Ddiff = messageIn.toFloat();  break;
	case 'g': Bluetooth.println(Ddiff, 5);  break;
	case 'H': Stiction = messageIn.toInt(); break;
	case 'h': Bluetooth.println(Stiction);  break;
	}
}
