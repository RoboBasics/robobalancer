#include <MPU9250_6Axis_MotionApps20.h>
#include <math.h>
#include "Wire.h"
#include "I2Cdev.h"

const double rad2degr         = 57.2957795;

// IMU ==============================================================================

MPU9250 imu;
I2Cdev   I2C_M;

bool dmpReady = false;  // set true if DMP init was successful

uint8_t devStatus;      // return status after each DMP operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

void setup() 
{
  Wire.begin();
  Serial.begin(115200); while (!Serial) {}
    
  imu.initialize();
  delay(1000);
  //Serial.println(imu.testConnection() ? "Connected" : "FAILED !!");
  int connection = imu.testConnection();
  if (connection == 1) { Serial.println("Connected"); }
  else { Serial.println("FAILED !!"); }

  devStatus = imu.dmpInitialize();
/*
  imu.setXAccelOffset(-2892);        // Chip default = -2880
  imu.setYAccelOffset(-1721);        // Chip default = -1741
  imu.setZAccelOffset(888);          // Chip default = 872
  imu.setXGyroOffset(31);            // Chip default = 0
  imu.setYGyroOffset(-12);           // Chip default = 0
  imu.setZGyroOffset(-15);           // Chip default = 0
*/
  if (devStatus == 0)
  {
    imu.setDMPEnabled(true);
    dmpReady = true;
    packetSize = imu.dmpGetFIFOPacketSize();  // get expected DMP packet size for later comparison
  }
  else
  {
    Serial.print(F("DMP Initialization failed (code "));
    Serial.print(devStatus);
    Serial.println(F(")"));
  }
  delay(15000);
}

void loop() 
{
  fifoCount = imu.getFIFOCount();
  if (fifoCount == 1024) { imu.resetFIFO(); }
  else
  {
    while (fifoCount < packetSize) { fifoCount = imu.getFIFOCount(); }
    imu.getFIFOBytes(fifoBuffer, packetSize);
    fifoCount -= packetSize;
    imu.dmpGetQuaternion(&q, fifoBuffer);
    imu.dmpGetGravity(&gravity, &q);
    imu.dmpGetYawPitchRoll(ypr, &q, &gravity);
  }

  double yawCurrent   = ypr[0]; yawCurrent   *= rad2degr; Serial.print(yawCurrent);   tab();
  double pitchCurrent = ypr[1]; pitchCurrent *= rad2degr; Serial.print(pitchCurrent); tab();
  double rollCurrent  = ypr[2]; rollCurrent  *= rad2degr; Serial.print(rollCurrent);  tab();
}

void tab() { Serial.print("\t"); }

