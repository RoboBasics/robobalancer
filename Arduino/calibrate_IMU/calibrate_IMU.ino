#include <SoftwareSerial.h>
#include "Wire.h"
#include "I2Cdev.h"
#include "MPU6050.h"
#include <math.h>
#include <TimedAction.h>

const double wheelRadius = 0.045;

MPU6050 accelgyro;
I2Cdev   I2C_M;

int16_t ax, ay, az;
int16_t gx, gy, gz;
//int16_t mx, my, mz;                    //Magnetometer not used for balancing

double filteredTiltAngle, linearImuSpeed;

TimedAction getIMUspeed = TimedAction(10, getImuSpeed);

unsigned long startTimeIMU, startTimeImuSpeed;

void setup() 
{
	Wire.begin();
	Serial.begin(115200);

	accelgyro.initialize();
	delay(1000);
	Serial.println(accelgyro.testConnection() ? "Connected" : "FAILED !!");

	startTimeIMU = millis();
}

void loop() 
{
	getIMUspeed.check();
	doSomethingElse();
}

void getImuSpeed()
{
	double currentTime = millis();
	double elapsedTime = (currentTime - startTimeIMU) * 0.001;
	startTimeImuSpeed = currentTime;

	accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

	double accelPitch = (double)ay / 16384;
	double accelTiltAngle = asin(-accelPitch);
	double gyroPitch = ((double)gx * 250 / 32768 + 0.84) * elapsedTime * PI / 180;
	filteredTiltAngle = 0.98 * (filteredTiltAngle + gyroPitch) + 0.02 * accelTiltAngle;

	linearImuSpeed = filteredTiltAngle / elapsedTime * wheelRadius;
	Serial.print("==================>");  Serial.print(filteredTiltAngle, 3); Serial.print("\n");
}

void doSomethingElse()
{
	double ticTac = millis();
	ticTac = (ticTac - startTimeIMU) * 0.00100;
	Serial.print(ticTac, 4); Serial.print("\n");
}
