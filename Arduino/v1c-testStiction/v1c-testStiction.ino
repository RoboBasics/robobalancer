/*==================================================================================
This script operates as 'firmware' for a RPi controlled balancer. The complete
solution also handles serial communication over usb with the Rpi

Functions:
- IMU (MPI6050) handling: gyroscope & accelerometer
- Encoder handling
- Motor handling (balancing, forward, backward and turning)
- Bluetooth communication for gain tuning, output monitoring and motion alternative control
- USB serial communication for command & status updates from/to Raspberry Pi

v1a - core balancing without Twiddle function                           (used with 'RB_core' apk-file for android)
v1b - core balancing with Twiddle function for automated gain tuning    (used with 'remote' apk-file for android)
v1c - as v1b with no yaw control and pitch gain at 1                    (used with 'stiction' apk-file for android)

Hardware used:
- Pololu Low Power DC motors (2282: 590 rpm, 9.7:1, 17 oz-in)
- Hall encoders 464.64 cpr (48 cpr after gear )
- Pololu wheels 120  x 60 mm
- Arduino UNO
- Seeed Studio 4A motor shield (Mind the pin settings; Seeed example library is wired wrong)
- Dagu bluetooth module (HC06)

The motor shield uses pin 9 & 10 as PWM pins, the encoders use pin 3 and 4 for
interrupts. So this configuration cannot handle servo's
(servo.h = timer1/D9+10, servoTimer2.h = timer2/D3+11)

The servo's of the robot camera and IR-radar will be controlled by Rpi through
an Ultraborg board.

Pinsettings ========================================================================

Uno

D0 = ---- (Rx)         D6 = motor IN2         D12 = ----     A0 = bluetooth TX
D1 = ---- (Tx)         D7 = motor IN3         D13 = ----     A1 = bluetooth RX
D2 = Right encoder A   D8 = motor IN4                        A2 = blue led
D3 = Left  encoder A   D9 = motor PWM A       SDA = n/a      A3 = red led
D4 = Right encoder B   D10 = motor PWM B      SCL = n/a      A4 = IMU   (SDA)
D5 = motor IN1         D11 = Left encoder B                  A5 = IMU   (SCL)

Motors =============================================================================

pololunr        : 2282
type            : 9.68:1LP
gearboxRatio    : 9.68
no load speed   : 590 rpm @ 6.0V (data sheet),  776.97 @ at 7.4V (Lipo)
ticksRevolution : 464.64
torque          : 17 oz/in

wheel diameter  : 0.12 m
wheel perimeter : 0.3769911  m
wheel distance  : 0.24 m  (center wheels)

no-load friction in pwm-values:

                   avg  min  used (after manual testing)
forward - left      16   13   15
forward - right     17   14    4
backward - left     17   13    5
backward - right    16   14   16

-----------------------------------------------------------------
=> ticksPerMeter    = ticks/revolution * wheel perimeter
=> ticksPerDegree	= ticks per revolution / 360
=> maxTicksPerSec   = RPM/60 * ticks/revolution
=> maxDegreesPerSec = RPM/60 * 360
=> degrees2pwm      = maxDegreesPerSec /255 * 0.02  (Adjusted to 50Hz)
=> yaw2pwm          = 2*Pi*wheel-distance*ticksPerMeter/360*max-ticks/sec/255   ;-0)

IMU ================================================================================

The Digital Motion Processor (DMP) of the MPU6050 is used:
- Because it's there
- Saves the effort of coding the angle and filter calculations
- Avoids CPU/memory use of the Arduino
- Performs self calibration

Downside: the self calibration requires a substantial initiation time

Note: The 'hack'of Jeff Rowberg only works with MPU6050 and MPU9150
      Although I adjusted the register settings, I couldn't get DMP 
	  working properly with a MPU9250 imu (So, no thanks to Invensense)

Messaging ==========================================================================

HardwareSerial for communication with the Raspberry Pi
SoftwareSerial for bluetooth communicating with the android remote

Control loop cascade ===============================================================

Outer loop: PD control horizontal (0.02s)       Inner loop: PD control vertical (0.02s)
-----------------------------------------       ---------------------------------------

target = required displacement (in degrees)
input  = ticks made by encoders
output = compensating ticks (in degrees)   -->  target  = required pitch angle
                                                input   = angle from IMU
                                                output  = compensating angle

A separate (PD) control loop is used for differential control

PID tuning =========================================================================

A header file for using a modified 'twiddle' tuning of the control loops has been added.
The header file is needed for using a struct object as parameter for functions (If the 
struct is declared in the main Arduino file, the sketch won't compile because functions 
are placed before type typedefinitions) Although it's a mere script-insert it best can 
be installed as an Arduino library for ease of use (#include). (Could of course also be 
done by using global variables) 

====================================================================================
Program size: 24.856 bytes (used 77% of a 32.256 byte maximum) (2,80 secs)
Minimum Memory Usage: 881 bytes (43% of a 2048 byte maximum)
====================================================================================*/

#include <twiddle.h>
#include <dataFilter.h>
#include <Encoder.h>
#include <math.h>
#include <TimedAction.h>
#include <SoftwareSerial.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include "I2Cdev.h"
#include <avr/wdt.h> 

// Pinsettings =====================================================================

const unsigned int MOTOR_PIN1  = 5;
const unsigned int MOTOR_PIN2  = 6;
const unsigned int MOTOR_PIN3  = 7;
const unsigned int MOTOR_PIN4  = 8;
const unsigned int SPEEDPIN_A  = 9;
const unsigned int SPEEDPIN_B  = 10;
const unsigned int LEDBLUEPIN  = A2;
const unsigned int LEDREDPIN   = A3;

// Speed ===========================================================================

const double rad2degr         = 57.2957795;

const double ticksPerMeter    = 1232.49588; //Robot dependant constants
const double ticksPerDegree   = 1.29066667;

const double maxTicksPerSec   = 6016.81696; //RPM dependant constants
const double maxDegreesPerSec = 4661.79000;
const double degrees2pwm      = 0.36563059; // = 18.2815294/sec at 50Hz
const double ticks2pwm        = 23.5953606;
const double yaw2pwm          = 0.21880007;

int speedTarget  = 0;
int turnTarget = 0;
int yawCompLeft = 0; int yawCompRight = 0;
double pwmLeft = 0.0; double pwmRight = 0.0;

int stictionFwLeft = 15; int stictionFwRight = 4;
int stictionBwLeft = 5;  int stictionBwRight = 16;

// Encoders =========================================================================

Encoder encMotorRight(2, 4);
Encoder encMotorLeft(3, 11);

int encValLeft, encValRight;
double ticksMade; 
int encDiff = 0;

// IMU ==============================================================================

MPU6050 imu;
I2Cdev   I2C_M;

bool dmpReady = false;  // set true if DMP init was successful

uint8_t devStatus;      // return status after each DMP operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

const double deadAngle = 30;
double pitchCurrent, pitchOld, yawCurrent, angleVelocity;

// Messaging ========================================================================

SoftwareSerial Bluetooth(A1, A0); // RX, TX

char messageIdBluetooth = ' ';
String messageInBluetooth = "";

const int BLUE = 1;
const int RED = 2;

// Timing ===========================================================================

boolean LOOPTIMER = false;

void setTimer() { LOOPTIMER = true; }

TimedAction ControlTimer = TimedAction(20, setTimer);

// Twiddle objects ==================================================================

/* 
                                        promessing gains:
   gain [3]                             4.29899 | 3.475 | 3.4
   step [3]                             0.29899 | 0.29  | 0.45
   state[3]                             0.08998 | 0.125 | 0.215 
   error, bestError
   stepSum, tolerance 
   idx
   
   Gains are initialized, rest will be initialized in resetRobot()
*/

twiddle vertical = { 1.0,  0.00,  0.0, 
					 0.0,      0.0,   0.0, 
					 0,        0,     0, 
					 0.0,      0.0,
					 0.0,      0.0,
					 0 };

/*twiddle horizontal =   { 0.0,  0.0, 0.0, 
						   0.0,  0.0, 0.0,
						   0, 0, 0,
						   0.0,  0.0,
						   0.0, 0.0,
						   0 };

/*twiddle differential = { 0.0,  0.0, 0.0, 
					  	   0.0,  0.0, 0.0,
						   0, 0, 0,
						   0.0,  0.0,
						   0.0, 0.0,
						   0 };
						 
*/

// Control loops ====================================================================

double pitchTarget, pitchError, pitchErrorSum, compAngle, maxVal, minVal;

double tickTarget, tickError, tickErrorSum, tickComp;
double KpHor  = 0.00000; double KdHor  = 0.00000;

double heading, yawError;
double KpYaw = 0.0; double KdYaw = 0.0;

// Filters ==========================================================================

dataFilter tickFilter(0.05);
//dataFilter yawFilter(0.35);

// State ============================================================================

boolean MESSAGE = false;
boolean FORWARD = false;
boolean TWIDDLE = false;


//===================================================================================

void setup()
{
	Wire.begin();
	Serial.begin(115200); while (!Serial) {}
	Bluetooth.begin(9600);

	pinMode(MOTOR_PIN1, OUTPUT);
	pinMode(MOTOR_PIN2, OUTPUT);
	pinMode(MOTOR_PIN3, OUTPUT);
	pinMode(MOTOR_PIN4, OUTPUT);
	pinMode(SPEEDPIN_A, OUTPUT);
	pinMode(SPEEDPIN_B, OUTPUT);
	pinMode(LEDBLUEPIN, OUTPUT);
	pinMode(LEDREDPIN,  OUTPUT);
		
	imu.initialize(); delay(1000);
	
	int connection = imu.testConnection();
	if (connection == 1) { blink(BLUE); }
	else { blink(RED); wdt_enable(WDTO_500MS); while (1) {} }

	devStatus = imu.dmpInitialize();

	// After calibration it turned out that the chip defaults gave the best results

	imu.setXAccelOffset(-2135);       // Chip default = -2135
	imu.setYAccelOffset(-560);        // Chip default = -560
	imu.setZAccelOffset(1580);        // Chip default = 1580
	imu.setXGyroOffset(0);            // Chip default = 0
	imu.setYGyroOffset(0);            // Chip default = 0
	imu.setZGyroOffset(0);            // Chip default = 0

	if (devStatus == 0)
	{
		imu.setDMPEnabled(true);
		dmpReady = true;
		packetSize = imu.dmpGetFIFOPacketSize();  // get expected DMP packet size for later comparison
		blink(BLUE);
	}
	else { blink(RED); wdt_enable(WDTO_500MS); while (1) {} }

	//stop();
	delay(15000);                                   // Setling time for the DMP
	blink(BLUE);
	resetRobot();
}

//===================================================================================

void loop()
{
	if (pitchCurrent <-deadAngle || pitchCurrent > deadAngle) { resetRobot(); }
	
	getMessage();                  // Check for user (bluetooth) or Raspberry commands 
	setSpeedTarget();              // Set target ticks for requested speed
	setTurnTarget();               // Adjust heading to requested angle to turn
	ControlTimer.check();          // Check if looptime has passed
	getAngles();                   // Read IMU, get pitch and yaw angle
	
	if (LOOPTIMER)                 // Enforce timing of 0.02 sec
	{
		getTicksMade();            // Read encoders
		horizontalControl();       // Compensate horizontal speed
		verticalControl();         // Compensate angular speed
		setDirection();            // Set motors to forward or backward
		differentialControl();     // Compensate for differential drift (or turn)
		setPower();                // Compute PWM value
		throttle();                // Rotate motors
		LOOPTIMER = false;
	}
}

/* =================================================================================
Control routines
==================================================================================== */

void verticalControl()
{
  pitchError   = pitchTarget - pitchCurrent;
  	
	if (TWIDDLE)
	{
		vertical.error = pitchError;                          //Will turm into an absolute value
		Process(vertical);
		if (vertical.stepSum <= vertical.tolerance) 
		{ 
			TWIDDLE = false; 
			ledOff(RED);
		}
	}
	
	double P = vertical.gain[0] * pitchError;

	pitchErrorSum += (pitchError * 0.02);
	pitchErrorSum = constrain(pitchErrorSum, minVal, maxVal);   //Clamp Iterm
    
	double I = vertical.gain[1] * pitchErrorSum; 

	double D = vertical.gain[2] * (angleVelocity / 0.02);       //angleVelocity = pitchCurrent - pitchOld

	compAngle = P + I - D;                                      //Derivative on Measurement!
	compAngle = constrain(compAngle, minVal, maxVal);           //Clamp output

	pitchOld = pitchCurrent;
}

void horizontalControl()
{
	angleVelocity = (pitchCurrent - pitchOld); 
	ticksMade = -1 * (ticksMade - angleVelocity); 
	ticksMade = tickFilter.process(ticksMade);
	tickError = (tickTarget - ticksMade);
	tickErrorSum += tickError * 0.02;
	if (tickErrorSum > 85.8) { tickErrorSum = 85.8; }            //maximum angle velocity
	if (tickErrorSum < -85.8) { tickErrorSum = -85.8; }          //i.e. 255*degrees2pwm
	pitchTarget = (KpHor * tickError) + (KdHor * tickErrorSum);
	if (pitchTarget > deadAngle) { pitchTarget = deadAngle; }
	if (pitchTarget < -deadAngle) { pitchTarget = -deadAngle; }
}

void differentialControl()
{
	
	if (yawCurrent < 180) { yawError = 0 - yawCurrent; }
	else { yawError = 360 - yawCurrent; }

	if (yawError <= 180) { yawError += heading; }  //Adjust actual to logical heading 0/360
	else { yawError -= heading; }
	
	if (yawError > 180) { yawError -= 360; }       //take the smallest error angle
	else if (yawError < -180) { yawError += 360; }
	
	double yawComp = (KpYaw * yawError); //+ (KdYaw * yawErrorSum);
	yawComp /= yaw2pwm;

	if (yawComp >= 0)                                // round-up to become an integer  
	{
		yawCompLeft = (yawComp + 0.5); yawCompRight = (yawComp + 0.5);
	}
	else 
	{
		yawCompLeft = (yawComp - 0.5); yawCompRight = (yawComp - 0.5);
	}
		
	if (FORWARD) { yawCompRight *= -1; } else { yawCompLeft *= -1; }
	//Serial.print("yawCompLeft"); tab();  Serial.println(yawCompLeft);
}

void setSpeedTarget()
{
	speedTarget = constrain(speedTarget, 0, 100);
	tickTarget = 0.01 * (double) speedTarget * 0.02 * maxDegreesPerSec;   // Percentage of max degrees at 50Hz
}

void setTurnTarget()
{
	//Construct turning function
	heading += turnTarget;
}

void setDirection()
{
	if (compAngle < 0) { forward(); resetEncoders(); }
	else if (compAngle > 0) { backward(); resetEncoders(); }
	else { neutral(); }
}

void setPower()
{
	if (compAngle < 0) { compAngle *= -1; }
	
	//Adjust for pitch error
	pwmLeft  = compAngle  / degrees2pwm;
	pwmRight = compAngle  / degrees2pwm;

	//Adjust for motor-offsets and stiction 
	if (FORWARD) 
	{
		pwmRight *= 1.0148; 
		pwmLeft  += stictionFwLeft; 
		pwmRight +=  stictionFwRight; 
	}
	else         
	{ 
		pwmLeft  *= 1.0083; 
		pwmRight *= 1.0834; 
		pwmLeft  += stictionBwLeft; 
		pwmRight += stictionBwRight; 
	}

	//Adjust for differential drift and/or angle to turn 
	pwmLeft += yawCompLeft; pwmRight += yawCompRight;
}

/* ===============================================================================
Sensor routines
================================================================================== */

void getAngles()
{
	fifoCount = imu.getFIFOCount();
	if (fifoCount == 1024) { imu.resetFIFO(); }
	else
	{
		while (fifoCount < packetSize) { fifoCount = imu.getFIFOCount(); }
		imu.getFIFOBytes(fifoBuffer, packetSize);
		fifoCount -= packetSize;
		imu.dmpGetQuaternion(&q, fifoBuffer);
		imu.dmpGetGravity(&gravity, &q);
		imu.dmpGetYawPitchRoll(ypr, &q, &gravity);
	}

	pitchCurrent = ypr[1]; pitchCurrent *= rad2degr;
	yawCurrent = ypr[0]; yawCurrent *= rad2degr;
	if (yawCurrent < 0) { yawCurrent += 360;  }        //adjust to range 0-360 
}

/* ===============================================================================
Encoder routines
================================================================================== */

void getTicksMade(void)
{
	encValRight = encMotorRight.read() * -1; encValLeft = encMotorLeft.read();

	if (encValRight >= 30000 || encValRight <= -30000) { resetEncoders(); } // Prevent Roll Over 
	if (encValLeft  >= 30000 || encValLeft  <= -30000) { resetEncoders(); }

	ticksMade = (encValLeft + encValRight) / 2;
	ticksMade /= ticksPerDegree;
	encDiff = abs(encValLeft) - abs(encValRight);
}

void resetEncoders(void)
{
	encMotorRight.write(0); encMotorLeft.write(0);
	encValRight = 0;        encValLeft = 0;
}

/* =================================================================================
Motor routines
==================================================================================== */

void throttle()
{
	int pwmL = int(pwmLeft + 0.5);
	pwmL = constrain(pwmL, 0, 255);
	int pwmR = int(pwmRight + 0.5);
	pwmR = constrain(pwmR, 0, 255);
	analogWrite(SPEEDPIN_B, pwmL);
	analogWrite(SPEEDPIN_A, pwmR);
}

void forward()
{
	FORWARD = true;
	digitalWrite(MOTOR_PIN1, LOW); digitalWrite(MOTOR_PIN2, HIGH);  // right motor
	digitalWrite(MOTOR_PIN3, HIGH);  digitalWrite(MOTOR_PIN4, LOW); // left motor
}

void backward()
{
	FORWARD = false;
	digitalWrite(MOTOR_PIN1, HIGH); digitalWrite(MOTOR_PIN2, LOW);  // right motor
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, HIGH); // left motor
}

void neutral()
{
	digitalWrite(MOTOR_PIN1, LOW);  digitalWrite(MOTOR_PIN2, LOW);
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, LOW);
}

void stop()
{
	analogWrite(SPEEDPIN_A, 0); analogWrite(SPEEDPIN_B, 0);
	neutral();
}

/* ==================================================================================
Communication routines
================================================================================== */

void getMessage()
{
	int messageIDSerial = 0;
	int messageOpSerial = 0;
	int value = 0;
	
	if (Serial.available() > 0)
	{
		int incomingSerialBytes = Serial.available();
		if (incomingSerialBytes == 5)
		{
			messageIDSerial = Serial.read()- 48;
			messageOpSerial = Serial.read()- 48;
			for (int i = 0; i < 3; i++)
			{
				value = value * 10 + Serial.read() - 48;
			}
			MESSAGE = true;
		}
		Serial.flush();
	}
	
	if (Bluetooth.available() > 0)
	{
		int incomingSerialBytes = Bluetooth.available();
		messageIdBluetooth = Bluetooth.read();
		messageInBluetooth = "";
		char incomingChar = Bluetooth.read();
		while (incomingChar != '\n')
		{
			messageInBluetooth = messageInBluetooth + incomingChar;
			incomingChar = Bluetooth.read();
		}
		processBluetoothMessage();
	}

	if (MESSAGE)
	{
		switch (messageIDSerial)
		{
			case 0: sendSensorValuesToRpi(); break;
			case 1: speedTarget = value; break; 
			case 2: setTurnTarget(); break;
			case 9: resetRobot; break;
		}
		MESSAGE = false;
	}
}

void processBluetoothMessage()
{

	switch (messageIdBluetooth)
	{
		case 'A': stictionFwLeft  = messageInBluetooth.toInt(); break;
		case 'B': stictionFwRight = messageInBluetooth.toInt(); break;
		case 'C': stictionBwLeft  = messageInBluetooth.toInt(); break;
		case 'D': stictionBwRight = messageInBluetooth.toInt(); break;

		case 'a': Bluetooth.println(stictionFwLeft);		    break;
		case 'b': Bluetooth.println(stictionFwRight);		    break;
		case 'c': Bluetooth.println(stictionBwLeft);		    break;
		case 'd': Bluetooth.println(stictionBwRight);		    break;

		case 'Z': Bluetooth.println(encDiff);				    break;
	}
}

void sendSensorValuesToRpi()
{
	// construct the sensor values; probably only room left for 1 distance sensor
}

void ledOn(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, HIGH); }
	else { digitalWrite(LEDREDPIN, HIGH); }
}

void ledOff(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, LOW); }
	else { digitalWrite(LEDREDPIN, LOW); }
}

void blink(int color)
{
	for (int x = 0; x < 5; x++) { ledOn(color); delay(100); ledOff(color); delay(100);}
}

void tab() { Serial.print("\t"); }    //Can be left-out in prod-version
void com() { Serial.print(","); }

/* ==================================================================================
Robot State routines
================================================================================== */

void resetRobot()
{
	stop();

	ledOff(BLUE); ledOff(RED);

	do
	{
		getAngles(); 
		getMessage();
		//delay(10);
	} while (pitchCurrent <-deadAngle || pitchCurrent > deadAngle);

	speedTarget    = 0;
	pitchTarget  = 0; compAngle = 0; pitchCurrent = 0; pitchOld = 0; pitchErrorSum = 0;
	tickTarget   = 0; tickComp = 0; ticksMade = 0; tickErrorSum  = 0;
	heading  = 0; yawCompLeft  = 0; yawCompRight = 0; 
	turnTarget = 0;

	resetEncoders();

	//Maxima for Errorsum and PID output 
	maxVal = 255 * degrees2pwm; minVal = -1 * maxVal;
	
	getAngles();

	if (TWIDDLE)
	{
		vertical.bestError = pitchError;
		if (vertical.bestError < 0) { vertical.bestError *= -1; }       // Absolute value 
		vertical.stepSum = 1.2;
		ledOn(RED);
	}
	
    heading = yawCurrent;                             //Take heading as offset to 0/360

	ledOn(BLUE);
}



