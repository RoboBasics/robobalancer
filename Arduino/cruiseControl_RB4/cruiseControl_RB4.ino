#include <Encoder.h>
#include <math.h>
#include <PID_v1.h>
#include <SoftwareSerial.h>
#include <TimedAction.h>

const unsigned int MOTORSHIELD_IN1 =  5;
const unsigned int MOTORSHIELD_IN2 =  6;
const unsigned int MOTORSHIELD_IN3 =  7;
const unsigned int MOTORSHIELD_IN4 =  8;
const unsigned int SPEEDPIN_A      =  9;
const unsigned int SPEEDPIN_B      = 10;

Encoder encMotorRight(2, 4);
Encoder encMotorLeft(3, 11);

SoftwareSerial Bluetooth(A0, A1); // RX, TX

const double maxMeterSec = 0.99;   // Theoretical 0.9896
const double tickMeter = 7953.503; // Axis, Wheels = 169.765 (48 tpr)
double maxTickSec, tickToPWM;

unsigned int powerLeft, powerRight;
int encValLeft, encValRight;
int tickRightLast = 0;
int tickLeftLast = 0;

int inputSpeedPerc = 0;
int powerStallLeft  = 55;  // nog checken mbv andere sketch
int powerStallRight = 58;
int powerInputLeft, powerInputRight, minPIDLimit, maxPIDLimit;

double meterSecLeft,tickMadeLeft, tickCompLeft, tickTarget;
double meterSecRight, tickMadeRight, tickCompRight;

PID PIDSpeedLeft(&tickMadeLeft, &tickCompLeft, &tickTarget, 0, 0, 0, DIRECT);
PID PIDSpeedRight(&tickMadeRight, &tickCompRight, &tickTarget, 0, 0, 0, DIRECT);

double KpSpeedLeft = 0.12; double KpSpeedRight = 0.11;
double KiSpeedLeft = 0.04; double KiSpeedRight = 0.05;
double KdSpeedLeft = 0.00; double KdSpeedRight = 0.00;

TimedAction timedControlSpeed = TimedAction(40, controlSpeed);

boolean runSwitch = true;

unsigned long startTimeIMU, startTimeEncoders;

void setup()
{ 
  Serial.begin(115200); while (!Serial) {}
  Bluetooth.begin(9600);

  pinMode(MOTORSHIELD_IN1, OUTPUT);
  pinMode(MOTORSHIELD_IN2, OUTPUT);
  pinMode(MOTORSHIELD_IN3, OUTPUT);
  pinMode(MOTORSHIELD_IN4, OUTPUT);
  pinMode(SPEEDPIN_A, OUTPUT);
  pinMode(SPEEDPIN_B, OUTPUT);
  
  stop();

  encMotorRight.write(0);
  encMotorLeft.write(0);

  maxTickSec = maxMeterSec * tickMeter;
  tickToPWM = maxTickSec / 255;             // In stead of map() 

  inputSpeedPerc = 30;
  inputSpeedPerc = constrain(inputSpeedPerc, 0, 100);
  double meterSecTarget = (double)inputSpeedPerc / 100 * maxMeterSec;
  tickTarget = meterSecTarget * tickMeter;
  minPIDLimit = -1 * tickTarget; maxPIDLimit = tickTarget;
  powerInputLeft = tickTarget; powerInputRight = tickTarget;
  
  PIDSpeedLeft.SetSampleTime(40);
  PIDSpeedLeft.SetMode(AUTOMATIC);
  PIDSpeedLeft.SetOutputLimits(minPIDLimit, maxPIDLimit);
  PIDSpeedRight.SetSampleTime(40);
  PIDSpeedRight.SetMode(AUTOMATIC);
  PIDSpeedRight.SetOutputLimits(minPIDLimit, maxPIDLimit);

  Forward();
  updateMotors();
  
  startTimeEncoders = millis();
}
void loop()
{
  updateGains();
  timedControlSpeed.check();
  updateMotors();
}

/* ================================================================================================ */
void controlSpeed(void) 
{
  readEncoders();
  PIDSpeedLeft.Compute();
  PIDSpeedRight.Compute();
  powerInputLeft = powerInputLeft + tickCompLeft;
  powerInputRight = powerInputRight + tickCompRight;
}
void updateMotors(void)
{
  powerLeft = powerInputLeft / tickToPWM; 
  powerLeft = constrain(powerLeft, 0 , 255);
  powerRight = powerInputRight / tickToPWM;
  powerRight = constrain(powerRight, 0, 255);
  driveMotors(powerLeft, powerRight);
}
void updateGains(void) 
{
	if (Bluetooth.available() > 0)
	{
		char incomingByte = Bluetooth.read();

		switch (incomingByte)
		{
		case 'e': KpSpeedLeft += 0.01;   break; 
		case 'r': KpSpeedLeft -= 0.01;   break; 
		case 'd': KiSpeedLeft += 0.01;   break; 
		case 'f': KiSpeedLeft -= 0.01;   break; 
		case 'c': KdSpeedLeft += 0.01;   break;
		case 'v': KdSpeedLeft -= 0.01;   break;

		case 'o': KpSpeedRight += 0.01;  break;
		case 'p': KpSpeedRight -= 0.01;  break;
		case 'k': KiSpeedRight += 0.01;  break;
		case 'l': KiSpeedRight -= 0.01;  break;
		case 'n': KdSpeedRight += 0.01;  break;
		case 'm': KdSpeedRight -= 0.01;  break;

		case '0': runSwitch = false;  runMode(); break;
		}
	}
	PIDSpeedLeft.SetTunings(KpSpeedLeft, KiSpeedLeft, KdSpeedLeft);
	PIDSpeedRight.SetTunings(KpSpeedRight, KiSpeedRight, KdSpeedRight);
}

/* ================================================================================================ 
   Encoder routines  
   ================================================================================================ */

void readEncoders(void)
{
  unsigned long currentTime = millis();
  float elapsedTime = (currentTime - startTimeEncoders) * 0.001;
  startTimeEncoders = currentTime;

  encValRight = encMotorRight.read(); encValLeft = encMotorLeft.read() * -1;

  tickMadeLeft = (encValLeft - tickLeftLast) / elapsedTime;
  tickMadeRight = (encValRight - tickRightLast) / elapsedTime;
  meterSecLeft = tickMadeLeft / tickMeter; meterSecRight = tickMadeRight / tickMeter;
  tickLeftLast = encValLeft; tickRightLast = encValRight;

  if (encValRight >= 30000 || encValRight <= -30000) { resetEncoders(); } // Prevent Roll Over 
  if (encValLeft  >= 30000 || encValLeft  <= -30000) { resetEncoders(); }
}
void resetEncoders(void)
{
  encMotorRight.write(0);
  encMotorLeft.write(0);
  tickRightLast = 0.0; tickLeftLast = 0.0;
}

/* ================================================================================================ 
   Motor routines  
   ================================================================================================ */

void driveMotors(int powerLeft, int powerRight) 
{
  analogWrite(SPEEDPIN_B, powerLeft);
  analogWrite(SPEEDPIN_A, powerRight);
}
void Forward() 
{
  digitalWrite(MOTORSHIELD_IN1, HIGH); digitalWrite(MOTORSHIELD_IN2, LOW);  // left motor
  digitalWrite(MOTORSHIELD_IN3, LOW);  digitalWrite(MOTORSHIELD_IN4, HIGH); // right motor
}
void Backward()
{
  digitalWrite(MOTORSHIELD_IN1, LOW);  digitalWrite(MOTORSHIELD_IN2, HIGH);
  digitalWrite(MOTORSHIELD_IN3, HIGH); digitalWrite(MOTORSHIELD_IN4, LOW);
}
void turnRight() 
{
  digitalWrite(MOTORSHIELD_IN1, HIGH); digitalWrite(MOTORSHIELD_IN2, LOW);
  digitalWrite(MOTORSHIELD_IN3, HIGH); digitalWrite(MOTORSHIELD_IN4, LOW);
}
void turnLeft() 
{
  digitalWrite(MOTORSHIELD_IN1, LOW);  digitalWrite(MOTORSHIELD_IN2, HIGH);
  digitalWrite(MOTORSHIELD_IN3, LOW);  digitalWrite(MOTORSHIELD_IN4, HIGH);
}
void stop() 
{
  analogWrite(SPEEDPIN_A, 0); analogWrite(SPEEDPIN_B, 0);
  digitalWrite(MOTORSHIELD_IN1, LOW);  digitalWrite(MOTORSHIELD_IN2, LOW);
  digitalWrite(MOTORSHIELD_IN3, LOW);  digitalWrite(MOTORSHIELD_IN4, LOW);
}
void runMode()
{
	stop();
	double savePowerInputLeft = powerInputLeft; double savePowerInputRight = powerInputRight;
	double saveTickCompLeft = tickCompLeft; double saveTickCompRight = tickCompRight;
	PIDSpeedLeft.SetMode(MANUAL); PIDSpeedRight.SetMode(MANUAL);
	while (runSwitch == false)
	{
		if (Bluetooth.available() > 0)
		{
			char incomingByte = Bluetooth.read();
			if (incomingByte = '1') { runSwitch = true; }
		}
	}
	encMotorRight.write(0); encMotorLeft.write(0);
	powerInputLeft = savePowerInputLeft; powerInputRight = savePowerInputRight;
	tickCompLeft = saveTickCompLeft; tickCompRight = saveTickCompRight;
	PIDSpeedLeft.SetMode(AUTOMATIC); PIDSpeedRight.SetMode(AUTOMATIC);
	Forward(); updateMotors(); startTimeEncoders = millis();
}
