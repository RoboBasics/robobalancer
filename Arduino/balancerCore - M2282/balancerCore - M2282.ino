/*==================================================================================
ToDo
@ Red led often fails while connection is made 
@ Working solution for reset on serial connection If there's one
@ Limit accelleration / increase of speed

This script operates as 'firmware' for a 2-wheeled balancer.

Bluetooth control (android app) is used for tuning and basic testing of turning & running.
A Raspberry Pi holds full control through serial communication

Functions:
- Vertical & differential control based on IMU (MPU6050) input (pitch & yaw);
- Horizontal control based on input from wheel encoders;
- Bluetooth communication for gain tuning, output monitoring and motion testing
- USB serial communication for commands & status updates from/to a Raspberry Pi
- Trimpots for gain tuning 
  (temporarely - removed in this version; use balancerCore - M2282 - trimPots.ino);

Hardware:
- Pololu Low Power DC motors (2282: 590 rpm, 9.68:1, 17 oz-in)
- Hall encoders 464.64 cpr (48 cpr after gear )
- Pololu wheels 120  x 60 mm (Wild Thumper)
- Arduino UNO
- Seeed Studio 4A motor shield (Mind the pin settings; Seeed example library contains wrong connections)
- Dagu bluetooth module (HC06)

The motor shield uses pin 9 & 10 as PWM pins, the encoders use pin 3 and 4 for
interrupts. So in this configuration the UNO cannot also handle servo's.
(servo.h = timer1/D9+10, servoTimer2.h = timer2/D3+11)

Pinsettings ========================================================================

D0 = ---- (Rx)         D6 = motor IN2         D12 = red led   A0 = bluetooth TX
D1 = ---- (Tx)         D7 = motor IN3         D13 = blue led  A1 = bluetooth RX
D2 = Right encoder A   D8 = motor IN4                         A2 =                     //trimpot 1 (D)
D3 = Left  encoder A   D9 = motor PWM A       SDA = n/a       A3 =                     //trimpot 2 (P)
D4 = Right encoder B   D10 = motor PWM B      SCL = n/a       A4 = IMU   (SDA)
D5 = motor IN1         D11 = Left encoder B                   A5 = IMU   (SCL)

Motors =============================================================================

pololunr        : 2282
type            : HP
gearboxRatio    : 9.68
no load speed   : 590 rpm @ 6.0V (data sheet),    756.30  @ at 7.9V (Lipo)
ticksRevolution : 464.64
torque          : 17 oz/in

wheel diameter  : 0.130 m 
wheel perimeter : 0.408 m
wheel distance  : 0.238 m  (center wheels)

average stiction in pwm-values (0-255):

                  no-load loaded   impl
forward  - left     28      20      10
forward  - right    12      24      15
backward - left     27      35      12
backward - right    10      42      25

IMU ================================================================================

The Digital Motion Processor (DMP) of the MPU6050 is used:
- Because it's there
- Saves the effort of coding the calculations for angle and filter
- Avoids CPU/memory use of the Arduino
- Performs self calibration

Constraint: Calibration requires a substantial initiation time (> 10 seconds)

Note: The 'hack'of Jeff Rowberg only works with MPU6050 and MPU9150
Although I adjusted the register settings, I couldn't get DMP
working properly with a MPU9250 imu (No thanks to Invensense information)

Messaging ==========================================================================

HardwareSerial for communication with the Raspberry Pi
SoftwareSerial for bluetooth communication with the android remote

Control loop cascade ===============================================================

Outer loop: PD control horizontal (0.01s)       Inner loop: PD control vertical (0.01s)
-----------------------------------------       ---------------------------------------

target = required displacement (in ticks)
input  = ticks made by encoders
output = compensating ticks (as angle)     -->  target  = required pitch angle
                                                input   = angle from IMU
                                                output  = compensating angle

A separate (PD) control loop is used for differential control

====================================================================================
Program size: 27.800 bytes (used 86% of a 32.256 byte maximum) (24,26 secs)
Minimum Memory Usage: 827 bytes (40% of a 2048 byte maximum)
====================================================================================*/

#include <Encoder.h>
#include <math.h>
#include <SoftwareSerial.h>
#include "MPU6050_6Axis_MotionApps20.h"
#include "Wire.h"
#include "I2Cdev.h"
#include <avr/wdt.h> 

// Pinsettings =====================================================================

const unsigned int MOTOR_PIN1 = 5;
const unsigned int MOTOR_PIN2 = 6;
const unsigned int MOTOR_PIN3 = 7;
const unsigned int MOTOR_PIN4 = 8;
const unsigned int SPEEDPIN_R = 9;
const unsigned int SPEEDPIN_L = 10;
const unsigned int LEDREDPIN  = 12;
const unsigned int LEDBLUEPIN = 13;

// Control parameters ==============================================================

const double rad2degr      = 57.2957795;
const double degree2pwm    = 18.2142857;   //255 / Max angle (14 degrees)

//Robot dependant constants
const double tickPerMeter  = 1137.68850;   //Ticks per revolution / wheel perimeter

//RPM and time dependant constants
const double maxTickPerInt = 58.56760096;  //RPM / 60 * ticksPerRevolution * 0.01 seconds
const double tick2degree =  0.00774793;    //     360 / ticksPerRevolution * 0.01 seconds

int turnTarget, speedTarget;
double speedOffset;

// Power ===========================================================================

//double yawComp;

int slackFwLeft = 10; int slackFwRight = 21;          //Made variable for tuning
int slackBwLeft = 12; int slackBwRight = 25;          //Made variable for tuning 

double pwmLeft, pwmRight;  

// Encoders =========================================================================

Encoder encMotorRight(3, 11);
Encoder encMotorLeft(2, 4);

int encValLeft, encValRight;
double ticksMade, ticksMadeOld, wheelVelocity, distanceMade, absDistanceMade, distanceTarget;

// IMU ==============================================================================

MPU6050 imu;
I2Cdev   I2C_M;

bool dmpReady = false;  // set true if DMP init was successful

uint8_t devStatus;      // return status after each DMP operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

Quaternion q;           // [w, x, y, z]         quaternion container
VectorFloat gravity;    // [x, y, z]            gravity vector
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

const double maxAngle  = 20.0;
const double pitchOffset = 0.8;	       	       //Only a variable during IMU calibration
const double cogOffset = -2.0;                 //Not necessary but makes balancing & start running more agile  -1.37

double pitchCurrent, pitchOld, yawCurrent, angleVelocity;

// Messaging ========================================================================

SoftwareSerial Bluetooth(A1, A0); // RX, TX

char marker = '\n';

const int BLUE = 1;
const int RED = 2;

// Timing ===========================================================================

double interval;
long timerLast;

// Control loops ====================================================================

double KpHor = 0.7195313; double KpPitch = 0.838761284;//0.83597733;
double KdHor = 1.0281250; double KdPitch = 0.038740000;//0.038738198;

double tickTarget, tickError;
double pitchTarget, pitchError, compAngle;
double yawTarget, yawError;

// State ============================================================================

boolean FORWARD   = false;

double actualSpeed, actualDistance;

char rbState = 'A';                  // info for Rpi values: U(p), D(own), R(unning), F(inished), A(ctivated)

//===================================================================================

void setup()
{
	Wire.begin();
	Serial.begin(921600); while (!Serial) {}
	Bluetooth.begin(9600); 

	pinMode(MOTOR_PIN1, OUTPUT);
	pinMode(MOTOR_PIN2, OUTPUT);
	pinMode(MOTOR_PIN3, OUTPUT);
	pinMode(MOTOR_PIN4, OUTPUT);
	pinMode(SPEEDPIN_R, OUTPUT);
	pinMode(SPEEDPIN_L, OUTPUT);
	pinMode(LEDBLUEPIN, OUTPUT);
	pinMode(LEDREDPIN,  OUTPUT);
	//pinMode(POT_P_PIN,  INPUT);
	//pinMode(POT_D_PIN,  INPUT);

	imu.initialize(); delay(1000);

	int connection = imu.testConnection();
	if (connection == 1) { blink(BLUE); }
	else { blink(RED); wdt_enable(WDTO_500MS); while (1) {} }

	devStatus = imu.dmpInitialize();
		
	imu.setXAccelOffset(-1765);                  // chip default: = -1684
	imu.setYAccelOffset(-2777);                  // chip default: = -2787
	imu.setZAccelOffset(1165);                   // chip default: =  1044
	imu.setXGyroOffset(96);                      // chip default: =     0
	imu.setYGyroOffset(-32);                     // chip default: =     0
	imu.setZGyroOffset(24);                      // chip default: =     0

	if (devStatus == 0)
	{
		imu.setDMPEnabled(true);
		dmpReady = true;
		packetSize = imu.dmpGetFIFOPacketSize();  // get expected DMP packet size for later comparison
		blink(BLUE);
	}
	else { blink(RED); wdt_enable(WDTO_500MS); while (1) {} }

	//stop();
	delay(15000);                     // Setling time for the DMP
	ledOn(BLUE);

	//wdt_enable(WDTO_2S);              // Set watchdog timer 

	resetRobot();
}

//===================================================================================

void loop()
{
	if (pitchCurrent <-maxAngle || pitchCurrent > maxAngle) { resetRobot(); }

	getMessage();                   // Check for user (bluetooth) or Raspberry commands 
	getTicksMade();                 // Read encoders
	getAngles();                    // Read IMU, get pitch and yaw angle

	long timerNow = millis();
	interval = ((double) timerNow - (double) timerLast) / 1000;
	
	if (interval >= 0.01)           // Enforce control loop timing
	{
		timerLast = timerNow;		
						
		horizontalControl();        // Compensate for target displacement
		verticalControl();          // Compensate for target vertical angle
		differentialControl();      // Compensate for target differential angle
		setPower();                 // Compute PWM value and throttle motors
	}
}

/* =================================================================================
Control routines
==================================================================================== */

inline void horizontalControl()
{
	double tickDiff = ticksMade - ticksMadeOld;
	distanceMade += tickDiff; 

	if (speedOffset < 0) { absDistanceMade = -distanceMade; }     //Enable negative (backward) distances; (abs() works for integers)
	else { absDistanceMade = distanceMade; }                      //Rather ugly but easy   

	if (distanceTarget == 0 || absDistanceMade >= distanceTarget) 
	{ 
	speedOffset = 0.0;
	distanceTarget = 0.0;
	rbState = 'F';
	}
	
	tickTarget += speedOffset;

	tickError = tickTarget - ticksMade;
	wheelVelocity = tickDiff / interval;
	double compTicks = KpHor * tickError - KdHor * wheelVelocity;  //Derivative on Measurement
	ticksMadeOld = ticksMade;

	pitchTarget = compTicks * tick2degree;
	pitchTarget += cogOffset;
}

inline void verticalControl()
{
	pitchError = pitchTarget - pitchCurrent;
	angleVelocity = (pitchCurrent - pitchOld) / interval;
	
	compAngle = KpPitch * pitchError - KdPitch * angleVelocity;     //Derivative on Measurement

	pitchOld = pitchCurrent;
}

inline void differentialControl()
{
	//Set error to a clockwise value in the range 0/360
	if (yawCurrent < 180) { yawError = 0 - yawCurrent; }
	else { yawError = 360.0 - yawCurrent; }
	
	//Adjust error by the target offset
	if (yawError <= 180) { yawError += yawTarget; }
	else { yawError -= yawTarget; }

	//Select the smallest error/angle-to-turn
	if (yawError > 180) { yawError -= 360.0; }
	else if (yawError < -180) { yawError += 360.0; }
}

inline void setPower()
{
	//Match pitch, velocity & drift and/or angle to turn
	pwmLeft = compAngle * degree2pwm - yawError;
	pwmRight = compAngle * degree2pwm + yawError;

	//Set direction and adjust for motor stiction and backlash
	if (pwmLeft < 0)  { forwardLeft();   pwmLeft *= -1;   pwmLeft += slackFwLeft; }
	else              { backwardLeft();                   pwmLeft += slackBwLeft; }
	if (pwmRight < 0) { forwardRight();  pwmRight *= -1;  pwmRight += slackFwRight; }
	else              { backwardRight();                  pwmRight += slackBwRight; }

	//Round-up to integer and cap to range
	int pwmL = int(pwmLeft + 0.5);  pwmL = constrain(pwmL, 0, 255);
	int pwmR = int(pwmRight + 0.5); pwmR = constrain(pwmR, 0, 255);

	//Throttle the motors
	analogWrite(SPEEDPIN_L, pwmL);
	analogWrite(SPEEDPIN_R, pwmR);
}

inline void setSpeed()
{
	double speedPerc = constrain((double)speedTarget, -100.0, 100.0) * 0.01; 
	speedOffset = speedPerc * maxTickPerInt;
}

inline void setDistance()
{
	distanceTarget *= tickPerMeter;
	distanceMade = 0;
	tickTarget = ticksMade;                                        //Eliminate residu-error from former leg/run
	ticksMadeOld = ticksMade;
	rbState = 'R';
}

inline void setTurn()
{
	//Adjust the target offset by the angle to turn
	yawTarget += (double)turnTarget;
	if (yawTarget > 360) { yawTarget -= 360.0; }
	else if (yawTarget < 0) { yawTarget += 360.0; }
	turnTarget = 0;
}

/* ===============================================================================
Sensor routines
================================================================================== */

inline void getAngles()
{
	fifoCount = imu.getFIFOCount();
	while (fifoCount < packetSize) { fifoCount = imu.getFIFOCount(); }

	imu.getFIFOBytes(fifoBuffer, packetSize);
	fifoCount -= packetSize;
	imu.dmpGetQuaternion(&q, fifoBuffer);
	imu.dmpGetGravity(&gravity, &q);
	imu.dmpGetYawPitchRoll(ypr, &q, &gravity);

	pitchCurrent = ypr[1] * rad2degr + pitchOffset;
	yawCurrent   = ypr[0] * rad2degr;

	if (yawCurrent < 0) { yawCurrent += 360; }        //adjust to (forward) clockwise range 0-360 
		
	imu.resetFIFO();
}

inline void getTicksMade(void)
{
	encValRight = encMotorRight.read() * -1; encValLeft = encMotorLeft.read();

	if (encValRight >= 30000 || encValRight <= -30000) { resetEncoders(); } // Prevent Roll Over 
	if (encValLeft >= 30000 || encValLeft <= -30000) { resetEncoders(); }

	ticksMade = ((double)encValLeft + (double)encValRight) / 2.0;
}

inline void resetEncoders(void)
{
	encMotorRight.write(0); encMotorLeft.write(0);
	encValRight = 0;        encValLeft = 0;

	ticksMadeOld = -1 * wheelVelocity;
	tickTarget = speedOffset;
}

/* =================================================================================
Motor routines
==================================================================================== */

inline void forwardLeft()
{
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, HIGH); // left motor
}

inline void forwardRight()
{
	digitalWrite(MOTOR_PIN1, HIGH); digitalWrite(MOTOR_PIN2, LOW);  // right motor
}

inline void backwardLeft()
{
	digitalWrite(MOTOR_PIN3, HIGH);  digitalWrite(MOTOR_PIN4, LOW); // left motor backwards
}

inline void backwardRight()
{
	digitalWrite(MOTOR_PIN1, LOW); digitalWrite(MOTOR_PIN2, HIGH);  // right motor backwards
}

void stop()
{
	analogWrite(SPEEDPIN_R, 0); analogWrite(SPEEDPIN_L, 0);
}

/* ==================================================================================
Interaction routines
================================================================================== */

inline void getMessage()
{
	//wdt_reset();                          //Prevent watchdog to reset the Arduino

	/* Used for trimpots
	//To prevent jitter give Sample & Hold capacitor of trimpots time to charge
	long timeNow = millis();
	if (timeNow - potTimeLast >= 20) { getTrimpotValues(); }
	*/
		
	if (Serial.available() > 0)
	{
		char messageIdRpi = Serial.read();
		String messageInRpi = Serial.readStringUntil(marker);
		processRpiMessage(messageIdRpi,messageInRpi);
	}
		
	//	Serial.flush();
	
	if (Bluetooth.available() > 0)
	{
		char messageIdBluetooth = Bluetooth.read();
		String messageInBluetooth = "";
		char incomingChar = Bluetooth.read();
		while (incomingChar != '\n')
		{
			messageInBluetooth = messageInBluetooth + incomingChar;
			incomingChar = Bluetooth.read();
		}
		processBluetoothMessage(messageIdBluetooth, messageInBluetooth);
	}

}

inline void processRpiMessage(char messageIdRpi, String messageInRpi)
{
	String outValue = "";
	char outCode = ' ';

	switch (messageIdRpi)
	{
		// Commands:
		case 'A': 
			KpPitch = messageInRpi.toFloat(); 
			break;
		case 'B': 
			KdPitch = messageInRpi.toFloat(); 
			break;
		case 'D': 
			distanceTarget = messageInRpi.toFloat(); 
			setDistance(); 
			break;
		case 'E': 
			KpHor = messageInRpi.toFloat(); 
			break;
		case 'F': 
			KdHor = messageInRpi.toFloat(); 
			break;
		case 'S': 
			speedTarget = messageInRpi.toInt(); 
			setSpeed(); 
			break;
		case 'T': 
			turnTarget = messageInRpi.toInt(); 
			setTurn(); 
			break;
		// Update requests
		case 'a': 
			outCode = 'a'; 
			outValue = String(KpPitch,5); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'b': 
			outCode = 'b'; 
			outValue = String(KdPitch,5); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'c': 
			outCode = 'C'; 
			ledOn(RED);
			outValue = " "; 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'd': 
			actualDistance = distanceMade / tickPerMeter;
			outCode = 'd'; 
			outValue = String(actualDistance,2); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'e': 
			outCode = 'e'; 
			outValue = String(KpHor,5); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'f': 
			outCode = 'f'; 
			outValue = String(KdHor,5); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'g': 
			outCode = 'g'; 
			outValue = String(pitchTarget,1); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'h': 
			outCode = 'h'; 
			outValue = String(yawTarget,1); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'm': 
			outCode = 'm'; 
			outValue = String(tickTarget, 1); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'j': 
			outCode = 'j'; 
			outValue = String(pitchError,5); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'l': 
			outCode = 'l'; 
			outValue = String(tickError, 5); 
			sendSerialMessage(outCode, outValue); 
			break;
		case 'k': 
			outCode = 'k'; 
			outValue = rbState; 
			sendSerialMessage(outCode, outValue); 
			break;
		case 's': 
			actualSpeed = wheelVelocity / tickPerMeter; 
			outCode = 's'; 
			outValue = String(actualSpeed);       
			sendSerialMessage(outCode, outValue); 
			break;
		case 'y':
			outCode = 'y';
			outValue = String(yawError, 1);
			sendSerialMessage(outCode, outValue);
	}
}

void sendSerialMessage(char outCode, String outValue)
{
	String messageOut = outCode + outValue + marker;
	Serial.print(messageOut);
}

inline void processBluetoothMessage(char messageIdBluetooth, String messageInBluetooth)
{

	switch (messageIdBluetooth)
	{
		case 'H': 
			speedTarget = messageInBluetooth.toInt(); 
			setSpeed();         
			break;
		case 'I': 
			distanceTarget = messageInBluetooth.toFloat(); 
			setDistance(); 
			break;
		case 'J': 
			turnTarget = messageInBluetooth.toInt();   
			setTurn();         
			break;

		//Following variables only used for tuning. Could be changed into constants

		case 'A': 
			KpPitch = messageInBluetooth.toFloat();       
			break;
		case 'C': 
			KdPitch = messageInBluetooth.toFloat();       
			break;
		case 'D': 
			KpHor = messageInBluetooth.toFloat();         
			break;
		case 'E': 
			KdHor = messageInBluetooth.toFloat();         
			break;
		//case 'F': KpYaw = messageInBluetooth.toFloat();         break;
		//case 'G': KdYaw = messageInBluetooth.toFloat();         break;

		//Following variables only used for tuning. Could be changed into constants

		case 'K': 
			slackFwLeft = messageInBluetooth.toInt();     
			break; 
		case 'L': 
			slackFwRight = messageInBluetooth.toInt();    
			break;
		case 'M': 
			slackBwLeft = messageInBluetooth.toInt();     
			break;
		case 'N': 
			slackBwRight = messageInBluetooth.toInt();    
			break;
		//case 'O': pitchOffset = messageInBluetooth.toFloat(); break; 
		//case 'P': potInput = messageInBluetooth.toInt();      break;

		//Following values are used for updating the Arduino screen

		case 'a': 
			Bluetooth.println(KpPitch, 3);      
			break;
		case 'c': 
			Bluetooth.println(KdPitch, 3);      
			break;
		case 'd': 
			Bluetooth.println(KpHor, 3);        
			break;
		case 'e': 
			Bluetooth.println(KdHor, 3);        
			break;
		case 'f': 
			Bluetooth.println(1.000);           
			break;                                             //Needed for the android app
		case 'g': 
			Bluetooth.println(0.000);           
			break;                                             //Needed for the android app
		case 'j': 
			Bluetooth.println(yawTarget, 3);    
			break;
		case 'k': 
			Bluetooth.println(slackFwLeft);     
			break;
		case 'l': 
			Bluetooth.println(slackFwRight);    
			break;
		case 'm': 
			Bluetooth.println(slackBwLeft);     
			break;
		case 'n': 
			Bluetooth.println(slackBwRight);    
			break;
		case 'o': 
			Bluetooth.println(pitchOffset, 2);  
			break;
		//case 'x': Bluetooth.println(pitchError, 3); break;
		case 'x': 
			Bluetooth.println(pitchCurrent, 3); 
			break;
	}
}

inline void ledOn(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, HIGH); }
	else { digitalWrite(LEDREDPIN, HIGH); }
}

void ledOff(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, LOW); }
	else { digitalWrite(LEDREDPIN, LOW); }
}

void blink(int color)
{
	for (int x = 0; x < 5; x++) { ledOn(color); delay(100); ledOff(color); delay(100); }
}

/* ==================================================================================
Robot State routines
================================================================================== */

void resetRobot()
{
	stop();

	ledOn(BLUE); rbState = 'D';

	// Initialize speed variables
	speedTarget = 0;
	turnTarget = 0;
	speedOffset = 0.0;

	// Initialize control loop variables
	tickTarget = 0.0; ticksMade = 0.0; ticksMadeOld = 0.0; wheelVelocity = 0.0;
	pitchTarget = cogOffset; pitchOld = 0.0; angleVelocity = 0.0; compAngle = 0.0;
	distanceTarget = 0.0; distanceMade = 0.0; speedOffset = 0.0;
	yawTarget = 0.0; yawError = 0.0;
	
	// Initialize power variables
	pwmLeft = 0.0; pwmRight = 0.0;

	// Initialize encoder variables
	resetEncoders();

	// Wait untill standing (almost) straight
	do
	{
		getAngles();
		getMessage();
	} while (pitchCurrent <-maxAngle || pitchCurrent > maxAngle);

	ledOff(BLUE); ledOff(RED); rbState = 'U';

	yawTarget = yawCurrent;                             //Take yawTarget as offset to 0/360
	pitchCurrent = 0.0;
	timerLast = millis(); interval = 0.0;               //Initialize control loop timer
}
