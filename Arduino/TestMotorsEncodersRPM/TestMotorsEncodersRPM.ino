/*
Functionality ======================================================================

- Test if the  motors and encoders are connected correctly 
- Find the maximum speed (RPM) per motor at a certain voltage (i.c. lipo 7.4)
- Find the stiction (minimum pwm to operate) per motor  

Not needed (but handy for checking calculations)
	- Find the maximum robot speed in ticks (depending on the decoder)
	- Find the maximum velocity in meters/second (depending on the wheel size)

Data ===============================================================================

M1 pololunr:2282|type:  9.68:1LP|gearboxRatio:  9.68|noLoadSpeed@6v:590 rpm|ticksRevolution:  464.64 |17 oz/in
M2 pololunr:2274|type:   100:1HP|gearboxRatio: 46.85|noLoadSpeed@6v:210 rpm|ticksRevolution: 2248.8  |80 oz/in
M3 pololunr:1101|type:100.37:1HP|gearboxRatio:100.37|noLoadSpeed@6v:320 rpm|ticksRevolution:   48    |30 oz/in
M4 pololunr 2271|type:  9.68:1HP|gearboxRatio:  9.68|noLoadSpeed@6v:990 rpm|ticksRevolution:  464.64 |39 oz/in

W1 = 120x60mm| diameter:0.120m| perimeter:0.3769911m 
W2 =  90x10mm| diameter:0.090m| perimeter:0.2827433m 
W3 =  42x19mm| diameter:0.045m| perimeter:0.1413717m

Values =============================================================================

=> tickMeter  = ticks/revolution * wheel perimeter
=> maxTickSec = RPM/60 * ticks/revolution
=> max velocity (m/s) = RPM/60 * wheel perimeter (or maxTickSec * tickMeter)

Approach ===========================================================================

1) Run at half PWM percentage and display encoder values to check if the right motor 
  turns in the right direction and the encoder ticks are counting correctly

2) Run at max PWM percentage in 4 cycles (left-fw, right-fw, left-bw, right-bw)
   - Measure ticks made per fixed interval (0.02 sec, i.e. 50 measuremnts per sec)
   - Skip every first 50 measurements 
   - Calculate the average ticks made per 20 sec (= 1000 measurements)
   - Display average ticks/interval, ticks/sec, RPM, Velocity
   - Reverse direction and run another cyle, display the values found
   - Calculate averages of both directions and display those

3) Increase power (0-64 % pwm) until the encoders registrate more than 2 ticks
   in 4 cycles (left-fw, right-fw, left-bw, right-bw

Note: the motors have to be free from the ground, therefor all values found are 
      no-load values   

Hardware used ======================================================================

- Arduino UNO
- Seeed Studio 4A motor shield

Pinsettings ========================================================================

D0 = ---- (Rx)         D6 = motor IN2         D12 = ----     A0 = bluetooth TX
D1 = ---- (Tx)         D7 = motor IN3         D13 = ----     A1 = bluetooth RX
D2 = Right encoder A   D8 = motor IN4                        A2 = blue led
D3 = Left  encoder A   D9 = motor PWM A       SDA = n/a      A3 = red led
D4 = Right encoder B   D10 = motor PWM B      SCL = n/a      A4 = IMU   (SDA)
D5 = motor IN1         D11 = Left encoder B                  A5 = IMU   (SCL)

====================================================================================

Program size: 8.376 bytes (used 26% of a 32.256 byte maximum) (6,11 secs)
Minimum Memory Usage: 719 bytes (35% of a 2048 byte maximum)

====================================================================================*/

#include <Encoder.h>
#include <math.h>
#include <TimedAction.h>

// Pinsettings ======================================================================

const unsigned int MOTOR_PIN1  = 5;
const unsigned int MOTOR_PIN2  = 6;
const unsigned int MOTOR_PIN3  = 7;
const unsigned int MOTOR_PIN4  = 8;
const unsigned int SPEEDPIN_R  = 9;
const unsigned int SPEEDPIN_L  = 10;

// Motors ===========================================================================

const double tickMeter = 1137.68850;   //for M1 & M4 + W1
//const double tickMeter = 7953.50302; //for M2 + W2
//const double tickMeter = 363.78273;  //for M2 + W2

const double ticksRevolution = 464.64;   //for 2271 & 2282
//const double ticksRevolution = 2248.8; //for 2274
//const double ticksRevolution = 48;     //for 1101

unsigned int pwmLeft, pwmRight;

// Encoders =========================================================================

Encoder encMotorRight(3, 11);
Encoder encMotorLeft(2, 4);

int encL, encR, encAbsL, encAbsR, encLOld, encROld, ticksMadeL, ticksMadeR;

// Timing ===========================================================================

TimedAction ControlTimer = TimedAction(20, setTimer);
long timeStart, timeStop;
double timeFw, timeBw;

// State ============================================================================

boolean MENU = true;
boolean LOOPCONTROL = false;
boolean FAILED      = true;
boolean RESPONSE    = false;

double ticksMadeTotalLeft, ticksMadeTotalRight,
maxTickSecLeft, maxTickSecRight, offsetLeftRight,
maxRpmLeft, maxRpmRight,
maxVelocityLeft, maxVelocityRight,
checkMaxTicksLeft, checkMaxTicksRight;

int count, cycle, skip, idx;
int sampleRounds = 0;

int sampleValues[100];
int sample = 0;

//===================================================================================

void setup()
{
	Serial.begin(115200); while (!Serial) {}

	pinMode(MOTOR_PIN1, OUTPUT);
	pinMode(MOTOR_PIN2, OUTPUT);
	pinMode(MOTOR_PIN3, OUTPUT);
	pinMode(MOTOR_PIN4, OUTPUT);
	pinMode(SPEEDPIN_L, OUTPUT);
	pinMode(SPEEDPIN_R, OUTPUT);
		
	stop();
	delay(1000);
}

//===================================================================================

void loop()
{
	while (1)
	{
		if (MENU)
		{
			Serial.println(" ");
			Serial.println("M ==> test motor connections ");
			Serial.println("R ==> get RPM values ");
			Serial.println("S ==> get stiction values ");
			Serial.println("");
			MENU = false;
		}
		getMessage(); 
	}
}

/* =================================================================================
Control routines
==================================================================================== */

void testMotorConnections()
{
	boolean MOVELEFT = true;
	boolean FORWARD = true;
	int enc = 1;

	for (int c = 0; c < 4; c++)
	{
		if (FORWARD)  { forward(); } else { backward(); }
		if (MOVELEFT) { enc = 1; pwmLeft = 100; pwmRight = 0; } 
		else          { enc = 2; pwmLeft = 0; pwmRight = 100; }
		resetEncoders();
		throttle();
		getResponse(enc);
		if (c < 1) { MOVELEFT = false; }
		else if (c < 2) { FORWARD = false; }
		else { MOVELEFT = true; }
	}
}

void getRpmValues()
{
	forward();
	pwmLeft = 255; pwmRight = 255;
	cycle = 0;
	Serial.println(""); Serial.println("FORWARD values:");
	while (cycle < 2)
	{
		resetValues();
		while (count < 200)
		{
			throttle();
			ControlTimer.check();
			if (LOOPCONTROL)
			{
				if (skip < 50) { skip += 1; }
				else if (skip == 50) { resetEncoders(); skip += 1; timeStart = millis(); }
				else
				{
					getTicksMade();
					ticksMadeTotalLeft += (double)ticksMadeL;
					ticksMadeTotalRight += (double)ticksMadeR;
					count += 1;
				}
				LOOPCONTROL = false;
			}
		}
		timeStop = millis();
		stop();
		computeRpmValues();
		displayRpmValues();
		cycle += 1;
		if (cycle < 2)
		{
			backward();
			Serial.println("BACKWARD values:");
		}
	}
}

void getStictionValues()
{
	getSampleRounds();

	for (int x = 0; x <= 24; x ++)
	{
		sampleValues[x] = 0;
	}
	
	sample = 0;
	while (sample <= sampleRounds * 4 - 1) 
	{
		int valuesFound[] = { 0,0,0,0 };
		stop();

		Serial.print("Taking samples, round # ");
		Serial.print(sample / 4 + 1);
		Serial.print(" of ");
		Serial.print(sampleRounds);


		for (int c1 = 0; c1 < 2; c1++)
		{
			Serial.print("\n"); 

			if (c1 < 1) { forward(); Serial.print(" Fw "); idx = 0; }
			else { backward(); Serial.print(" Bw "); idx = 2; }

			stop();
			resetEncoders();
			delay(200);
			boolean FOUNDL = false; boolean FOUNDR = false;

			for (int c2 = 10; c2 < 100; c2++)
			{
				pwmLeft = c2; pwmRight = c2;

				if (!FOUNDL || !FOUNDR)
				{
					Serial.print("|");
					throttle();
					delay(400);
					getTicksMade();

					encAbsL = abs(encL); encAbsR = abs(encR);
					if (encAbsL > 10 && !FOUNDL) { valuesFound[idx] = c2; FOUNDL = true; }
					if (encAbsR > 10 && !FOUNDR) { valuesFound[idx + 1] = c2; FOUNDR = true; }
						
					if ( FOUNDL && FOUNDR )
					{
						String filler = "";
						for (int t = c2; t <= 65; t++) { filler += " "; }
						Serial.print(filler);
						Serial.print(" L = "); Serial.print(valuesFound[idx]); 
						Serial.print(" R = "); Serial.print(valuesFound[idx+1]);
						stop();
					}
				}
			}
		}
		for (int t = 0; t <= 3; t ++)
		{
			sampleValues[sample] = valuesFound[t];
			sample += 1;
		}
		Serial.print("\n");
	}
	stop();
	Serial.println(" ");
	Serial.print("Lf"); tab();  
	Serial.print("Lb"); tab();
	Serial.print("Rf"); tab();
	Serial.println("Rb ");

	int printCnt = 0;
	while (printCnt < sample)
	{
		Serial.print(sampleValues[printCnt]); tab();
		Serial.print(sampleValues[printCnt + 2]); tab();
		Serial.print(sampleValues[printCnt + 1]); tab();
		Serial.println(sampleValues[printCnt + 3]);
		printCnt += 4;
	}
}


void setTimer()
{
	LOOPCONTROL = true;
}

void computeRpmValues()
{
	checkMaxTicksLeft = ticksMadeTotalLeft / ((timeStop - timeStart) / 1000);
	checkMaxTicksRight = ticksMadeTotalRight / ((timeStop - timeStart) / 1000);
	
	ticksMadeTotalLeft /= 200; 
	ticksMadeTotalRight /= 200;
	offsetLeftRight = ticksMadeTotalLeft / ticksMadeTotalRight;
	maxTickSecLeft = ticksMadeTotalLeft / 0.02; 
	maxTickSecRight = ticksMadeTotalRight / 0.02;
	maxRpmLeft = (maxTickSecLeft / ticksRevolution) * 60;
	maxRpmRight = (maxTickSecRight / ticksRevolution) * 60;
	maxVelocityLeft = maxTickSecLeft / tickMeter;
	maxVelocityRight = maxTickSecRight / tickMeter;
	
}

void displayRpmValues()
{
	
	Serial.print("Max ticks/sec  L-R + Offset  = "); tab(); 
	Serial.print(maxTickSecLeft, 7); tab();
	Serial.print(maxTickSecRight, 7); tab();
	Serial.println(offsetLeftRight, 7);
	
	Serial.print("RPM            L-R           = "); tab();
	Serial.print(maxRpmLeft); tab();
	Serial.println(maxRpmRight);

	Serial.print("Check Max ticks/sec L-R      = "); tab();
	Serial.print(checkMaxTicksLeft); tab();
	Serial.println(checkMaxTicksRight);

	/*
	Serial.print("Max velocity   L-R           = "); tab(); 
	Serial.print(maxVelocityLeft); tab();
	Serial.println(maxVelocityRight);
	*/
}

void resetValues()
{
	resetEncoders();
	encLOld = 0;      encROld = 0;
	ticksMadeTotalLeft = 0; ticksMadeTotalRight = 0;
	maxTickSecLeft = 0;     maxTickSecRight = 0; 
	maxRpmLeft = 0;         maxRpmRight = 0;
	offsetLeftRight = 0;
	count = 0; skip = 0;
}

/* ===============================================================================
Encoder routines
================================================================================== */

void getTicksMade(void)
{
	encR = encMotorRight.read() * -1; encL = encMotorLeft.read();

	if (encR >= 30000 || encR <= -30000) { resetEncoders(); } // Prevent Roll Over 
	if (encL  >= 30000 || encL  <= -30000) { resetEncoders(); }

	ticksMadeL  = encL  - encLOld;
	ticksMadeR = encR - encROld;
	encLOld  = encL;
	encROld = encR;
}

void resetEncoders(void)
{
	encMotorRight.write(0);                encMotorLeft.write(0);
	encR = 0;                       encL = 0;
	encROld = -1 * ticksMadeR;  encLOld = -1 * ticksMadeL;
}

/* =================================================================================
Motor routines
==================================================================================== */

void throttle()
{
		analogWrite(SPEEDPIN_R, pwmRight);
		analogWrite(SPEEDPIN_L, pwmLeft);
}

void backward()
{
	digitalWrite(MOTOR_PIN1, LOW); digitalWrite(MOTOR_PIN2, HIGH);  // right motor
	digitalWrite(MOTOR_PIN3, HIGH);  digitalWrite(MOTOR_PIN4, LOW); // left motor
}

void forward()
{
	digitalWrite(MOTOR_PIN1, HIGH); digitalWrite(MOTOR_PIN2, LOW);  // right motor
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, HIGH); // left motor
}

void stop()
{
	analogWrite(SPEEDPIN_L, 0); analogWrite(SPEEDPIN_R, 0);
	//digitalWrite(MOTOR_PIN1, LOW);  digitalWrite(MOTOR_PIN2, LOW);
	//digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, LOW);
}

/* ==================================================================================
Communication routines
================================================================================== */

void getMessage()
{
	if (Serial.available() > 0)
	{
		int incomingSerialBytes = Serial.available();
		char incomingChar = Serial.read();
		switch (incomingChar)
		{
		case 'M': testMotorConnections(); MENU = true; break;
		case 'm': testMotorConnections(); MENU = true; break;
		case 'R': getRpmValues();         MENU = true; break;
		case 'r': getRpmValues();         MENU = true; break;
		case 'S': getStictionValues();    MENU = true; break;
		case 's': getStictionValues();    MENU = true; break;
		case 'Y': RESPONSE = true; FAILED = false; break;
		case 'y': RESPONSE = true; FAILED = false; break;
		case 'N': RESPONSE = true; FAILED = true; break;
		case 'n': RESPONSE = true; FAILED = true; break;
		}
		Serial.flush();
	}
}

void getResponse(int encId)
{
	RESPONSE = false;
	while (!RESPONSE)
	{
		getMessage();
		throttle();
		getTicksMade();
		switch (encId)
		{
		case 1: Serial.print("Left ");  tab(); Serial.print(encL); tab(); break;
		case 2: Serial.print("Right "); tab(); Serial.print(encR); tab(); break;
		}
		Serial.println("Correct Y/N?");
	}
	stop();
	if (FAILED) { Serial.println("Test failed"); while (FAILED) { delay(10000); } }
}

void getSampleRounds()
{
	Serial.print("\n");
	Serial.println("Enter number of sample rounds (max 25)?");
	boolean ANSWER = false;
	String messageIn = "";
	while (!ANSWER)
	{
		if (Serial.available() > 0)
		{
			int incomingSerialBytes = Serial.available();
			/*
			char incomingChar = Serial.read();
			messageIn = messageIn + incomingChar;
			while (len(messageIn) < incomingSerialBytes){ }
			{
				messageIn = messageIn + incomingChar;
				incomingChar = Serial.read();
			}
			sampleRounds = messageIn.toInt();
			*/

			messageIn = Serial.readString();
			sampleRounds = messageIn.toInt();

			if (sampleRounds < 1 || sampleRounds > 25)
			{
				Serial.println("Number should be between 1 and 25");
			}
			else
			{
				Serial.println(" ");
				ANSWER = true;
			}
		}
	}
}

void tab() { Serial.print("\t"); }
void com() { Serial.print(","); }
