/*
Simple library to filter data complementary (current value and previous value)
Created as library for usage with various sensors
Ofcourse free to use and replicate 

insert URL

RoboBASics 2016, March 24
*/

#ifndef dataFilter_h
#define dataFilter_h

class dataFilter
{
public:
	dataFilter(double filterGain);
	//~dataFilter();
	double process(double newData);
	void reset();
private:
	double filterValues[2];
	double gainValue;
	double result;
};
#endif
