/*
insert URL
*/

#include <dataFilter.h>

dataFilter::dataFilter(double filterGain)
{
	gainValue = filterGain;
	filterValues[0] = 0.0;
	filterValues[1] = 0.0;
}

double dataFilter::process(double newData)
{
	result = 0.0;
	filterValues[0] = newData*gainValue + filterValues[1] * (1 - gainValue);
	filterValues[1] = filterValues[0];
	result = filterValues[1];
	return result;
}
void dataFilter::reset()
{
	filterValues[1] = 0.0;
}
