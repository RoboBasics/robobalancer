#include <dataFilter.h>


dataFilter filterAngle(0.25);
dataFilter filterSpeed(0.35);


double inputArray[40];

void setup()
{
	Serial.begin(115200); while (!Serial) {}
	inputArray[0] = 0.1;
	for(int t = 1; t <= 39; t++){ inputArray[t] = inputArray[t-1] + 0.05;}
}

void loop()
{
	for(int k = 0; k <= 39; k++) 
	{
		double input = inputArray[k];
		double outcome = filterAngle.process(input);
		Serial.print(input); Serial.print(", "); Serial.print(outcome); Serial.print(", ");
		outcome = filterSpeed.process(input);
		Serial.println(outcome);
	}
}