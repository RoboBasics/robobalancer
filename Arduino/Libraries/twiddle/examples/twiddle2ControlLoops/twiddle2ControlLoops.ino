/*
Example using 2 PID loops:
= PD loop (vertical)
= PID loop (horizontal)

Output (last line):
417  17.10508155 1.52100014  1.61050820  0.00000000  0.09999992  14.09999084 0.49999952  1.20999932  0.09999989  0.09999989
 */

#include <twiddle.h>

double PvertOld, IvertOld, DvertOld, PhorOld, IhorOld, DhorOld;

int twiddleCount = 0;

/*                        P          I        D         steps       action   errors   sum  */
twiddle vertical   = { 2.250000, 0.000000, 0.10000, 1.0, 0.0, 0.1, 0, 0, 0, 0.0, 0.0, 1.0 };
twiddle horizontal = { 1.050000, 0.100000, 0.10000, 1.0, 0.1, 0.1, 0, 0, 0, 0.0, 0.0, 1.0 };

void setup()
{
	Serial.begin(115200);
	vertical.error   = (vertical.gain[0]   * 10 + vertical.gain[1]   * 10 + vertical.gain[2]   * 10);
  horizontal.error = (horizontal.gain[0] * 10 + horizontal.gain[1] * 10 + horizontal.gain[2] * 10);
	Reset(vertical);
  Reset(horizontal);
}

void loop()
{
	if (
	    vertical.gain[0]   != PvertOld || 
	    vertical.gain[1]   != IvertOld || 
	    vertical.gain[2]   != DvertOld ||
      horizontal.gain[0] != PhorOld || 
      horizontal.gain[1] != IhorOld || 
      horizontal.gain[2] != DhorOld
	    )
  {
		PvertOld = vertical.gain[0];   IvertOld = vertical.gain[1];   DvertOld = vertical.gain[2];
    PhorOld  = horizontal.gain[0]; IhorOld  = horizontal.gain[1]; DhorOld  = horizontal.gain[2];
    
    twiddleCount += 1;
		
		Process(vertical); Process (horizontal);
    
    Serial.print(twiddleCount);          tab(); Serial.print(vertical.error, 8);       tab(); Serial.print(vertical.bestError, 8); tab(); 
		Serial.print(vertical.gain[0], 8);   tab(); Serial.print(vertical.gain[1], 8);     tab(); Serial.print(vertical.gain[2], 8); tab();
    Serial.print(horizontal.error, 8);   tab(); Serial.print(horizontal.bestError, 8); tab();
    Serial.print(horizontal.gain[0], 8); tab(); Serial.print(horizontal.gain[1], 8);   tab(); Serial.println(horizontal.gain[2], 8);
    
		vertical.error   = (vertical.gain[0]   * 10 + vertical.gain[1]   * 10 + vertical.gain[2] * 10);
    horizontal.error = (horizontal.gain[0] * 10 + horizontal.gain[1] * 10 + horizontal.gain[2] * 10);
	}
}
 void tab()
 {
  Serial.print("\t");
 }


