/* 
Help file for using twiddle tuning with control loops (PID)

Needed for using a struct object as parameter for the functions 
(If the struct is declared in the main Arduino file, the sketch won't compile
 because functions are placed before type typedefinitions)

Although it's a mere script-insert it best canb be installed as an Arduino library for ease of use

Of course free for use!

RoboBASics, 2016/07/06
*/

struct twiddle
{
	double gain[3];
	double step[3];
	int action[3];
	double error;
	double bestError;
	double stepSum;
};

void Process(twiddle &obj)
{
	if (obj.error < 0) { obj.error *= -1; }                                              // Absolute error for balancing also creates negative values
	obj.stepSum = obj.step[0] + obj.step[1] + obj.step[2];
	if (obj.stepSum > 0.000001)
	{
		for (int i = 0; i < 3; i++)
		{
			if (obj.action[i] < 1)
			{
				obj.gain[i] += obj.step[i];
				obj.action[i] = 1;
			}
			else if (obj.action[i] < 2)
			{
				if (obj.error < obj.bestError)
				{
					obj.bestError = obj.error;
					obj.step[i] *= 1.1;
					obj.gain[i] += obj.step[i];
				}
				else
				{
					obj.gain[i] -= 2 * obj.step[i];
					if (obj.gain[i] < 0) { obj.gain[i] = 0 + obj.step[i] / 10; }    // Gains must always be >= 0 
					obj.action[i] = 2;
				}
			}
			else if (obj.action[i] < 3)
			{
				if (obj.error < obj.bestError)
				{
					obj.bestError = obj.error;
					obj.step[i] *= 1.1;
					obj.gain[i] += obj.step[i];
					obj.action[i] = 1;
				}
				else
				{
					obj.gain[i] += obj.step[i];
					obj.step[i] *= 0.9;
					obj.action[i] = 0;
				}
			}
		}
	}
}

void Reset (twiddle &obj)
{
	obj.bestError = obj.error;
	for (int t=0; t< 3;t++){ obj.action[t] = 0; }
}
