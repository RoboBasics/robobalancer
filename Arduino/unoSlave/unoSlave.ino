#include "U8glib.h"
#include <SoftwareSerial.h>

U8GLIB_SH1106_128X64 u8g(U8G_I2C_OPT_DEV_0 | U8G_I2C_OPT_FAST);	// Dev 0, Fast I2C / TWI

SoftwareSerial Uno1(A1, A0);      // RX, TX

String message = "";
char messageId = ' ';
char* valuesStr[] = 
{
	"......ap","......ai","......ad","......sp","......si","......sd","..l","..r","  Init   ","  IDLE   "
};

boolean CONNECTED = false;
boolean NewMessage = false;

void getSerialInput()
{
	if (Uno1.available() > 0)
	{
		NewMessage = true;
		messageId = ' ';
		int incomingSerialBytes = Uno1.available();
		Serial.println(incomingSerialBytes);
		messageId = Uno1.read();
		Serial.println(messageId);
		message = "";
		int i = 2;
		while (i <= incomingSerialBytes)
		{
			char incomingChar = Uno1.read();
			message = message + incomingChar;
			i += 1;
		}
		Serial.println(message);
	}
}

void processMessage()
{
	switch (messageId)
	{
  		case '1': message.toCharArray(valuesStr[0], 9); break;
  		case '2': message.toCharArray(valuesStr[1], 9); break;
  		case '3': message.toCharArray(valuesStr[2], 9); break;
  		case '4': message.toCharArray(valuesStr[3], 9); break;
  		case '5': message.toCharArray(valuesStr[4], 9); break;
  		case '6': message.toCharArray(valuesStr[5], 9); break;
  		case '7': message.toCharArray(valuesStr[6], 4); break;
  		case '8': message.toCharArray(valuesStr[7], 4); break;
  		case '9': message.toCharArray(valuesStr[8], 10); break;
  		case '!': message.toCharArray(valuesStr[9], 10); break;
	}
}

void draw(void) 
{
	// graphic commands to redraw the complete screen should be placed here 
 
	u8g.setFont(u8g_font_6x13);
	
	u8g.drawStr(0, 10, "P");
	u8g.drawStr(12, 10, (char *)(valuesStr[0]));
	u8g.drawStr(0, 20, "I");
	u8g.drawStr(12, 20, (char *)(valuesStr[1]));
	u8g.drawStr(0, 30, "D");
	u8g.drawStr(12, 30, (char *)(valuesStr[2]));
	u8g.drawHLine(0, 32, 64);
	u8g.drawVLine(64, 0, 64);
	u8g.drawStr(0, 44, "P");
	u8g.drawStr(12, 44, (char *)(valuesStr[3]));
	u8g.drawStr(0, 54, "I");
	u8g.drawStr(12, 54, (char *)(valuesStr[4]));
	u8g.drawStr(0, 64, "D ");
	u8g.drawStr(12, 64, (char *)(valuesStr[5]));
	u8g.drawStr(70, 10, "St-L");
	u8g.drawStr(100, 10, (char *)(valuesStr[6]));
	u8g.drawStr(70, 20, "St-R");
	u8g.drawStr(100, 20, (char *)(valuesStr[7]));
	u8g.drawHLine(64, 22, 64);
	u8g.drawStr(70, 39, (char *)(valuesStr[8]));
	u8g.drawHLine(64, 46, 64);
	u8g.drawStr(70, 62, (char *)(valuesStr[9]));
}

void execScreen()
{
	u8g.firstPage();
	do 
	{
		draw();
	} 
	while (u8g.nextPage());
}

void setup() 
{
	Uno1.begin(115200); 

	// Send Serial Ready ============================================================
	Uno1.print('R');
	/*while (!CONNECTED)
	{
		char incomingByte = Uno1.read();
		if (incomingByte == 'M') { CONNECTED = true; }
	}*/
	
	u8g.sleepOff();

	// Flip screen ===================================================================
	u8g.setRot180();
	
	// Assign default color value  ===================================================
	if (u8g.getMode() == U8G_MODE_R3G3B2) { u8g.setColorIndex(255); }                   // white
	else if (u8g.getMode() == U8G_MODE_GRAY2BIT) { u8g.setColorIndex(3); }              // max intensity
	else if (u8g.getMode() == U8G_MODE_BW) { u8g.setColorIndex(1); }                    // pixel on
	else if (u8g.getMode() == U8G_MODE_HICOLOR) { u8g.setHiColorByRGB(255, 255, 255); }
	execScreen();
}

void loop() {
  getSerialInput();
  if (NewMessage)
  { 
	processMessage();
	NewMessage = false;
	execScreen();
	Uno1.print('R');
	Serial.print("responsed");
  }
  else { execScreen(); }
}
