/*
Pinsettings ========================================================================

D0 = ---- (Rx)         D6  = motor PWM right   D12 = motor IN3 right  A0 = ----
D1 = ---- (Tx)         D7  = encoder B left    D13 = motor IN4 right  A1 = ----
D2 = encoder A right   D8  = motor IN1 left                           A2 = ----
D3 = encoder A left    D9  = servo PWM 1       SDA = n/a              A3 = ----
D4 = encoder B right   D10 = servo PWM 2       SCL = n/a              A4 = ----
D5 = motor PWM left    D11 = motor IN2 left                           A5 = ----
*/

// connect motor controller pins to Arduino digital pins
// motor one

const unsigned int MOTOR_PIN1 = 5;
const unsigned int MOTOR_PIN2 = 6;
const unsigned int MOTOR_PIN3 = 7;
const unsigned int MOTOR_PIN4 = 8;
const unsigned int SPEEDPIN_A = 9;
const unsigned int SPEEDPIN_B = 10;

const unsigned int LEDBLUEPIN = A2;
const unsigned int LEDREDPIN = A3;
const int BLUE = 1;
const int RED = 2;

void setup()
{
	// set all the motor control pins to outputs
	Serial.begin(115200); while (!Serial) {}

	pinMode(MOTOR_PIN1, OUTPUT);
	pinMode(MOTOR_PIN2, OUTPUT);
	pinMode(MOTOR_PIN3, OUTPUT);
	pinMode(MOTOR_PIN4, OUTPUT);
	pinMode(SPEEDPIN_A, OUTPUT);
	pinMode(SPEEDPIN_B, OUTPUT);
	pinMode(LEDBLUEPIN, OUTPUT);
	pinMode(LEDREDPIN, OUTPUT);
}

void loop()
{
	blink(BLUE);
	forward();
	analogWrite(SPEEDPIN_B, 128);
	delay(1000);
	stop();
	blink(RED);
	analogWrite(SPEEDPIN_A, 128);
	delay(1000);
	stop();
}

void forward()
{
	digitalWrite(MOTOR_PIN1, LOW); digitalWrite(MOTOR_PIN2, HIGH);  // right motor
	digitalWrite(MOTOR_PIN3, HIGH);  digitalWrite(MOTOR_PIN4, LOW); // left motor
}

void backward()
{
	digitalWrite(MOTOR_PIN1, HIGH); digitalWrite(MOTOR_PIN2, LOW);  // right motor
	digitalWrite(MOTOR_PIN3, LOW);  digitalWrite(MOTOR_PIN4, HIGH); // left motor
}

void stop()
{
	analogWrite(SPEEDPIN_A, 0); analogWrite(SPEEDPIN_B, 0);
}

void ledOn(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, HIGH); }
	else { digitalWrite(LEDREDPIN, HIGH); }
}

void ledOff(int color)
{
	if (color == BLUE) { digitalWrite(LEDBLUEPIN, LOW); }
	else { digitalWrite(LEDREDPIN, LOW); }
}

void blink(int color)
{
	for (int x = 0; x < 5; x++) { ledOn(color); delay(100); ledOff(color); delay(100); }
}
